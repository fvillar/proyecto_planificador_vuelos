require('dotenv').config();
const jwt = require('jsonwebtoken');

const { getConnection } = require('../db');
const { generateError } = require('../helpers');

async function userIsAuthenticated(req, res, next) {
  let connection;

  try {
    // Comprobamos la cabecera de autenticación
    const { authorization } = req.headers;

    if (!authorization) {
      throw generateError('Falta la cabecera de Authorization', 401);
    }

    //Extraemos el token de la cabecera de autenticación
    const authorizationParts = authorization.split(' ');

    let token;

    if (authorizationParts.length === 1) {
      token = authorization;
    } else if (authorizationParts[0] === 'Bearer') {
      token = authorizationParts[1];
    } else {
      throw generateError('Token no legible', 401);
    }

    //validamos el token
    let decoded;
    try {
      decoded = jwt.verify(token, process.env.SECRET);
    } catch (error) {
      throw generateError('El token no es válido', 401);
    }

    // Comprobamos que la fecha de expedición del token es mayor a la
    // fecha de última actualización de password del usuario
    const { id, iat } = decoded;

    connection = await getConnection();

    const [
      result
    ] = await connection.query(
      'SELECT fecha_cambio_password FROM usuarios WHERE id=?',
      [id]
    );

    if (!result.length) {
      throw generateError(
        'El usuario del token no existe en la base de datos',
        404
      );
    }

    const [user] = result;

    // Tener en cuenta que el iat del token está guardado en segundos y node trabaja en
    // milisegundos.
    if (new Date((iat + 7200) * 1000) < new Date(user.fecha_cambio_password)) {
      throw generateError(
        'El token ya no vale, haz login para conseguir otro',
        401
      );
    }

    req.auth = decoded;
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

function userIsAdmin(req, res, next) {
  if (!req.auth || req.auth.rol !== 'administrador') {
    const error = generateError('No tienes privilegios de administración', 401);
    return next(error);
  }

  next();
}

module.exports = {
  userIsAuthenticated,
  userIsAdmin
};
