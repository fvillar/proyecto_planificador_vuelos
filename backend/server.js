require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const path = require('path');

const app = express();
const port = process.env.PORT;

//CONTROLLERS
//USER CONTROLLERS
const {
  newUser,
  getAllUsers,
  allInfoUser,
  validateUser,
  updatePasswordUser,
  loginUser,
  basicInfoUser,
  editUser,
  deleteUser,
  disableUser,
  enableUser,
  recoveryPassword
} = require('./controllers/users');

//TICKET CONTROLLERS
const {
  newTicket,
  getAllTickets,
  getTicketsUser,
  getTicket,
  editTicket,
  deleteTicket
} = require('./controllers/tickets');

//FLIGHT CONTROLLERS
const {
  newFlight,
  getAllFlights,
  getFlightsByAllParams,
  getFlightsAirline,
  getFlightsAirport,
  getFlightsUser,
  getFlight,
  editFlight,
  deleteFlight
} = require('./controllers/flights');

//AIRLINE CONTROLLERS
const {
  newAirline,
  getAllAirlines,
  getAirline,
  editAirline,
  deleteAirline,
  disableAirline,
  enableAirline
} = require('./controllers/airlines');

//AIRPORT CONTROLLERS
const {
  newAirport,
  getAllAirports,
  getAirport,
  editAirport,
  deleteAirport,
  disableAirport,
  enableAirport,
  getAirportCities
} = require('./controllers/airports');

// MIDDLEWARES DE AUTENTICACION
const {
  userIsAuthenticated,
  userIsAdmin
} = require('./middlewares/authentications');

//MIDDLEWARE COMUNES
//loguer
app.use(morgan('dev'));
//body parsing json
app.use(bodyParser.json());
//multipart parsing
app.use(fileUpload());
//use cors
app.use(cors());
// Serve static
app.use(express.static(path.join(__dirname, 'static')));

//ROUTES
//USER ROUTES
app.post('/users', newUser);
app.get('/users', userIsAuthenticated, userIsAdmin, getAllUsers);
app.get('/users/all/:id', userIsAuthenticated, allInfoUser);
app.get('/users/validate', validateUser);
app.put('/users/disable/:id', userIsAuthenticated, disableUser);
app.put('/users/enable', enableUser);
app.put('/users/recovery', recoveryPassword);
app.put('/users/password/:id', userIsAuthenticated, updatePasswordUser);
app.post('/users/login', loginUser);
app.get('/users/:id', userIsAuthenticated, basicInfoUser);
app.put('/users/:id', userIsAuthenticated, editUser);
app.delete('/users/:id', userIsAuthenticated, userIsAdmin, deleteUser);

//TICKET ROUTES
app.post('/tickets', userIsAuthenticated, newTicket);
app.get('/tickets/all', userIsAuthenticated, userIsAdmin, getAllTickets);
app.get('/tickets/user/:id', userIsAuthenticated, getTicketsUser);
app.get('/tickets/:id_vuelo', userIsAuthenticated, getTicket);
app.put('/tickets', userIsAuthenticated, userIsAdmin, editTicket);
app.delete('/tickets', userIsAuthenticated, userIsAdmin, deleteTicket);

//FLIGHT ROUTES
app.post('/flights', userIsAuthenticated, userIsAdmin, newFlight);
app.get('/flights/all', getAllFlights);
app.get(
  '/flights/byparams',
  userIsAuthenticated,
  userIsAdmin,
  getFlightsByAllParams
);
app.get('/flights/airline/:codigo', userIsAuthenticated, getFlightsAirline);
app.get('/flights/airport/:codigo', userIsAuthenticated, getFlightsAirport);
app.get('/flights/user/:id', userIsAuthenticated, getFlightsUser);
app.get('/flights/:id', userIsAuthenticated, getFlight);
app.put('/flights/:id', userIsAuthenticated, editFlight);
app.delete('/flights/:id', userIsAuthenticated, userIsAdmin, deleteFlight);

//AIRLINES ROUTES
app.post('/airlines', userIsAuthenticated, userIsAdmin, newAirline);
app.get('/airlines', userIsAuthenticated, getAllAirlines);
app.delete('/airlines/:id', userIsAuthenticated, userIsAdmin, deleteAirline);
app.put(
  '/airlines/disable/:id',
  userIsAuthenticated,
  userIsAdmin,
  disableAirline
);
app.put(
  '/airlines/enable/:id',
  userIsAuthenticated,
  userIsAdmin,
  enableAirline
);
app.put('/airlines/:id', userIsAuthenticated, userIsAdmin, editAirline);
app.get('/airlines/:id', userIsAuthenticated, getAirline);

//AIRPORT ROUTES

app.post('/airports', userIsAuthenticated, userIsAdmin, newAirport);
app.get('/airports', userIsAuthenticated, getAllAirports);
app.get('/airports/cities', getAirportCities);
app.delete('/airports/:id', userIsAuthenticated, userIsAdmin, deleteAirport);
app.put(
  '/airports/disable/:id',
  userIsAuthenticated,
  userIsAdmin,
  disableAirport
);
app.put(
  '/airports/enable/:id',
  userIsAuthenticated,
  userIsAdmin,
  enableAirport
);
app.put('/airports/:id', userIsAuthenticated, userIsAdmin, editAirport);
app.get('/airports/:id', userIsAuthenticated, getAirport);

//ERROR MIDDLEWARE
app.use((error, req, res, next) => {
  console.log(error);
  console.log(error.message);
  res.status(error.httpCode || 500).send({
    status: 'error',
    message: error.message
  });
});

//NOT FOUND MIDDLEWARE
app.use((req, res) => {
  res.status(404).send({ status: 'error', messagge: 'Página no encontrada' });
});

//SERVER LISTEN
app.listen(port, () => {
  console.log(`Servidor funcionando en localhost:${port} 🚀`);
});
