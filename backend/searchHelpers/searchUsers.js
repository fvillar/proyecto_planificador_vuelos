require('dotenv').config();

function searchUsers(queryParams) {
  let query = `SELECT * FROM usuarios`;
  const params = [];

  const {
    email,
    rol,
    activo,
    nombre,
    apellido_1,
    apellido_2,
    tipo_identificacion,
    numero_identificacion,
    direccion,
    codigo_postal,
    ciudad,
    provincia,
    pais,
    telefono,
    numero_vuelos_min,
    numero_vuelos_max
  } = queryParams;

  if (
    email ||
    rol ||
    activo ||
    nombre ||
    apellido_1 ||
    apellido_2 ||
    tipo_identificacion ||
    numero_identificacion ||
    direccion ||
    codigo_postal ||
    ciudad ||
    provincia ||
    pais ||
    telefono ||
    numero_vuelos_min ||
    numero_vuelos_max
  ) {
    query = `${query} WHERE `;
    const conditions = [];

    if (email) {
      conditions.push('email = ?');
      params.push(email);
    }

    if (rol) {
      conditions.push('rol=?');
      params.push(rol);
    }

    if (activo) {
      conditions.push('activo=?');
      params.push(activo);
    }

    if (nombre) {
      conditions.push('nombre like ?');
      params.push(`%${nombre}%`);
    }

    if (apellido_1) {
      conditions.push('apellido_1 like ?');
      params.push(`%${apellido_1}%`);
    }

    if (apellido_2) {
      conditions.push('apellido_2 like ?');
      params.push(`%${apellido_2}%`);
    }

    if (tipo_identificacion) {
      conditions.push('tipo_identificacion=?');
      params.push(tipo_identificacion);
    }

    if (numero_identificacion) {
      conditions.push('numero_identificacion=?');
      params.push(numero_identificacion);
    }

    if (direccion) {
      conditions.push('direccion like ?');
      params.push(`%${direccion}%`);
    }

    if (codigo_postal) {
      conditions.push('codigo_postal=?');
      params.push(codigo_postal);
    }

    if (ciudad) {
      conditions.push('ciudad like ?');
      params.push(`%${ciudad}%`);
    }

    if (provincia) {
      conditions.push('provincia like ?');
      params.push(`%${provincia}%`);
    }

    if (pais) {
      conditions.push('pais=?');
      params.push(pais);
    }

    if (telefono) {
      conditions.push('telefono like ?');
      params.push(`%${telefono}%`);
    }

    if (numero_vuelos_min) {
      conditions.push('numero_vuelos>=?');
      params.push(numero_vuelos_min);
    }

    if (numero_vuelos_max) {
      conditions.push('numero_vuelos<=?');
      params.push(numero_vuelos_max);
    }

    query = `${query} ${conditions.join(' AND ')}`;
  }

  return {
    query,
    params
  };
}

module.exports = {
  searchUsers
};
