require('dotenv').config();

function searchFlights(queryParams) {
  let query = `SELECT 
  v.id as id_vuelo, v.codigo codigo_vuelo, v.fecha_salida as fecha_salida, v.fecha_llegada as fecha_llegada, v.hora_salida as hora_salida, v.hora_llegada as hora_llegada, v.numero_escalas as numero_escalas, v.precio as precio, v.duracion as duracion,
  apo.id as id_aeropuerto_origen, apo.codigo as codigo_aeropuerto_origen, apo.nombre as nombre_aeropuerto_origen, apo.ciudad as ciudad_aeropuerto_origen,
  apd.id as id_aeropuerto_destino, apd.codigo as codigo_aeropuerto_destino, apd.nombre as nombre_aeropuerto_destino, apd.ciudad as ciudad_aeropuerto_destino,
  al.id as id_aerolinea, al.codigo as codigo_aerolinea, al.nombre as nombre_aerolinea
  FROM vuelos v
JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
JOIN aerolineas al ON v.id_aerolinea=al.id`;
  const params = [];

  const {
    origen,
    destino,
    fecha_salida,
    duracion,
    numero_escalas,
    precio_min,
    precio_max
  } = queryParams;

  if (
    origen ||
    destino ||
    fecha_salida ||
    duracion ||
    numero_escalas ||
    precio_min ||
    precio_max
  ) {
    query = `${query} WHERE `;
    const conditions = [];

    if (origen) {
      conditions.push('apo.ciudad=?');
      params.push(origen);
    }

    if (destino) {
      conditions.push('apd.ciudad=?');
      params.push(destino);
    }

    if (fecha_salida) {
      conditions.push('v.fecha_salida=?');
      params.push(fecha_salida);
    }

    if (duracion) {
      const minutes = duracion * 60;
      conditions.push('v.duracion<?');
      params.push(minutes);
    }

    if (numero_escalas) {
      conditions.push('v.numero_escalas<=?');
      params.push(numero_escalas);
    }

    if (precio_min) {
      conditions.push('v.precio >= ?');
      params.push(precio_min);
    }

    if (precio_max) {
      conditions.push('v.precio <= ?');
      params.push(precio_max);
    }

    query = `${query} ${conditions.join(' AND ')}`;
  }

  return { query, params };
}

function searchFlightsIdaVuelta(queryParams) {
  const { query, params } = searchFlights(queryParams);

  let queryVuelta = `SELECT 
  v.id as id_vuelo, v.codigo codigo_vuelo, v.fecha_salida as fecha_salida, v.fecha_llegada as fecha_llegada, v.hora_salida as hora_salida, v.hora_llegada as hora_llegada, v.numero_escalas as numero_escalas, v.precio as precio, v.duracion as duracion,
  apo.id as id_aeropuerto_origen, apo.codigo as codigo_aeropuerto_origen, apo.nombre as nombre_aeropuerto_origen, apo.ciudad as ciudad_aeropuerto_origen,
  apd.id as id_aeropuerto_destino, apd.codigo as codigo_aeropuerto_destino, apd.nombre as nombre_aeropuerto_destino, apd.ciudad as ciudad_aeropuerto_destino,
  al.id as id_aerolinea, al.codigo as codigo_aerolinea, al.nombre as nombre_aerolinea
  FROM vuelos v
JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
JOIN aerolineas al ON v.id_aerolinea=al.id`;
  const paramsVuelta = [];

  const {
    origen,
    destino,
    fecha_vuelta,
    duracion,
    numero_escalas,
    precio_min,
    precio_max
  } = queryParams;

  if (
    origen ||
    destino ||
    fecha_vuelta ||
    duracion ||
    numero_escalas ||
    precio_min ||
    precio_max
  ) {
    queryVuelta = `${queryVuelta} WHERE `;
    const conditionsVuelta = [];

    if (origen) {
      conditionsVuelta.push('apd.ciudad=?');
      paramsVuelta.push(origen);
    }

    if (destino) {
      conditionsVuelta.push('apo.ciudad=?');
      paramsVuelta.push(destino);
    }

    if (fecha_vuelta) {
      conditionsVuelta.push('v.fecha_salida=?');
      paramsVuelta.push(fecha_vuelta);
    }

    if (duracion) {
      const minutes = duracion * 60;
      conditionsVuelta.push('v.duracion<?');
      paramsVuelta.push(minutes);
    }

    if (numero_escalas) {
      conditionsVuelta.push('v.numero_escalas<=?');
      paramsVuelta.push(numero_escalas);
    }

    if (precio_min) {
      conditionsVuelta.push('v.precio >= ?');
      paramsVuelta.push(precio_min);
    }

    if (precio_max) {
      conditionsVuelta.push('v.precio <= ?');
      paramsVuelta.push(precio_max);
    }

    queryVuelta = `${queryVuelta} ${conditionsVuelta.join(' AND ')}`;
  }

  return { query, params, queryVuelta, paramsVuelta };
}

function fullSearchFlights(queryParams) {
  let query = `SELECT 
  v.id as id_vuelo, 
  v.codigo codigo_vuelo, 
  v.fecha_salida as fecha_salida, 
  v.fecha_llegada as fecha_llegada, 
  v.hora_salida as hora_salida, 
  v.hora_llegada as hora_llegada, 
  v.numero_escalas as numero_escalas, 
  v.precio as precio, 
  v.duracion as duracion,
  apo.id as id_aeropuerto_origen, 
  apo.codigo as codigo_aeropuerto_origen, 
  apo.nombre as nombre_aeropuerto_origen, 
  apo.ciudad as ciudad_aeropuerto_origen,
  apo.pais as pais_aeropuerto_origen,
  apd.id as id_aeropuerto_destino, 
  apd.codigo as codigo_aeropuerto_destino, 
  apd.nombre as nombre_aeropuerto_destino, 
  apd.ciudad as ciudad_aeropuerto_destino,
  apd.pais as pais_aeropuerto_destino,
  al.id as id_aerolinea, 
  al.codigo as codigo_aerolinea, 
  al.nombre as nombre_aerolinea
  FROM vuelos v
JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
JOIN aerolineas al ON v.id_aerolinea=al.id
LEFT JOIN billetes b ON v.id=b.id_vuelo
LEFT JOIN usuarios u ON b.id_usuario=u.id`;
  const params = [];

  const {
    ciudad_origen,
    ciudad_destino,
    fecha_salida,
    codigo_vuelo,
    id_aerolinea,
    codigo_aerolinea,
    nombre_aerolinea,
    id_aeropuerto_origen,
    codigo_aeropuerto_origen,
    nombre_aeropuerto_origen,
    id_aeropuerto_destino,
    codigo_aeropuerto_destino,
    nombre_aeropuerto_destino,
    id_usuario,
    email_usuario,
    numero_identificacion_usuario,
    pais_usuario
  } = queryParams;

  if (
    ciudad_origen ||
    ciudad_destino ||
    fecha_salida ||
    codigo_vuelo ||
    id_aerolinea ||
    codigo_aerolinea ||
    nombre_aerolinea ||
    id_aeropuerto_origen ||
    codigo_aeropuerto_origen ||
    nombre_aeropuerto_origen ||
    id_aeropuerto_destino ||
    codigo_aeropuerto_destino ||
    nombre_aeropuerto_destino ||
    id_usuario ||
    email_usuario ||
    numero_identificacion_usuario ||
    pais_usuario
  ) {
    query = `${query} WHERE `;
    const conditions = [];

    if (ciudad_origen) {
      conditions.push('apo.ciudad like ?');
      params.push(`%${ciudad_origen}%`);
    }

    if (ciudad_destino) {
      conditions.push('apd.ciudad like ?');
      params.push(`%${ciudad_destino}%`);
    }

    if (fecha_salida) {
      conditions.push('v.fecha_salida=?');
      params.push(fecha_salida);
    }

    if (codigo_vuelo) {
      conditions.push('v.codigo=?');
      params.push(codigo_vuelo);
    }

    if (id_aerolinea) {
      conditions.push('al.id=?');
      params.push(id_aerolinea);
    }

    if (codigo_aerolinea) {
      conditions.push('al.codigo=?');
      params.push(codigo_aerolinea);
    }

    if (nombre_aerolinea) {
      conditions.push('al.nombre like ?');
      params.push(`%${nombre_aerolinea}%`);
    }

    if (id_aeropuerto_origen) {
      conditions.push('apo.id=?');
      params.push(id_aeropuerto_origen);
    }

    if (codigo_aeropuerto_origen) {
      conditions.push('apo.codigo=?');
      params.push(codigo_aeropuerto_origen);
    }

    if (nombre_aeropuerto_origen) {
      conditions.push('apo.nombre like ?');
      params.push(`%${nombre_aeropuerto_origen}%`);
    }

    if (id_aeropuerto_destino) {
      conditions.push('apd.id=?');
      params.push(id_aeropuerto_destino);
    }

    if (codigo_aeropuerto_destino) {
      conditions.push('apd.codigo=?');
      params.push(codigo_aeropuerto_destino);
    }

    if (nombre_aeropuerto_destino) {
      conditions.push('apd.nombre like ?');
      params.push(`%${nombre_aeropuerto_destino}%`);
    }

    if (id_usuario) {
      conditions.push('u.id=?');
      params.push(id_usuario);
    }

    if (email_usuario) {
      conditions.push('u.email like ?');
      params.push(`%${email_usuario}%`);
    }

    if (numero_identificacion_usuario) {
      conditions.push('u.numero_identificacion=?');
      params.push(numero_identificacion_usuario);
    }

    if (pais_usuario) {
      conditions.push('u.pais=?');
      params.push(pais_usuario);
    }

    query = `${query} ${conditions.join(' AND ')}`;
  }

  return { query, params };
}

module.exports = {
  searchFlights,
  searchFlightsIdaVuelta,
  fullSearchFlights
};
