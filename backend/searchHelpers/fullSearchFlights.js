require('dotenv').config();

function fullSearchFlights(queryParams) {
  let query = `SELECT 
  v.id as id_vuelo, 
  v.codigo codigo_vuelo, 
  v.fecha_salida as fecha_salida, 
  v.fecha_llegada as fecha_llegada, 
  v.hora_salida as hora_salida, 
  v.hora_llegada as hora_llegada, 
  v.numero_escalas as numero_escalas, 
  v.precio as precio, 
  v.duracion as duracion,
  apo.id as id_aeropuerto_origen, 
  apo.codigo as codigo_aeropuerto_origen, 
  apo.nombre as nombre_aeropuerto_origen, 
  apo.ciudad as ciudad_aeropuerto_origen,
  apo.pais as pais_aeropuerto_origen
  apd.id as id_aeropuerto_destino, 
  apd.codigo as codigo_aeropuerto_destino, 
  apd.nombre as nombre_aeropuerto_destino, 
  apd.ciudad as ciudad_aeropuerto_destino,
  apd.pais as pais_aeropuerto_destino
  al.id as id_aerolinea, 
  al.codigo as codigo_aerolinea, 
  al.nombre as nombre_aerolinea
  FROM vuelos v
JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
JOIN aerolineas al ON v.id_aerolinea=al.id
JOIN billetes b ON v.id=b.id_vuelo
JOIN usuarios u ON b.id_usuario=u.id`;
  const params = [];

  const {
    ciudad_origen,
    ciudad_destino,
    fecha_salida,
    codigo_vuelo,
    id_aerolinea,
    codigo_aerolinea,
    nombre_aerolinea,
    id_aeropuerto_origen,
    codigo_aeropuerto_origen,
    nombre_aeropuerto_origen,
    id_aeropuerto_destino,
    codigo_aeropuerto_destino,
    nombre_aeropuerto_destino,
    id_usuario,
    email_usuario,
    numero_identificacion_usuario,
    pais_usuario
  } = queryParams;

  if (
    ciudad_origen ||
    ciudad_destino ||
    fecha_salida ||
    codigo_vuelo ||
    id_aerolinea ||
    codigo_aerolinea ||
    nombre_aerolinea ||
    id_aeropuerto_origen ||
    codigo_aeropuerto_origen ||
    nombre_aeropuerto_origen ||
    id_aeropuerto_destino ||
    codigo_aeropuerto_destino ||
    nombre_aeropuerto_destino ||
    id_usuario ||
    email_usuario ||
    numero_identificacion_usuario ||
    pais_usuario
  ) {
    query = `${query} WHERE `;
    const conditions = [];

    if (ciudad_origen) {
      conditions.push('apo.ciudad like ?');
      params.push(`%${ciudad_origen}%`);
    }

    if (ciudad_destino) {
      conditions.push('apd.ciudad like ?');
      params.push(`%${ciudad_destino}%`);
    }

    if (fecha_salida) {
      conditions.push('v.fecha_salida=?');
      params.push(fecha_salida);
    }

    if (codigo_vuelo) {
      conditions.push('v.codigo=?');
      params.push(codigo_vuelo);
    }

    if (id_aerolinea) {
      conditions.push('al.id=?');
      params.push(id_aerolinea);
    }

    if (codigo_aerolinea) {
      conditions.push('al.codigo=?');
      params.push(codigo_aerolinea);
    }

    if (nombre_aerolinea) {
      conditions.push('al.nombre like ?');
      params.push(`%${nombre_aerolinea}%`);
    }

    if (id_aeropuerto_origen) {
      conditions.push('apo.id=?');
      params.push(id_aeropuerto_origen);
    }

    if (codigo_aeropuerto_origen) {
      conditions.push('apo.codigo=?');
      params.push(codigo_aeropuerto_origen);
    }

    if (nombre_aeropuerto_origen) {
      conditions.push('apo.nombre like ?');
      params.push(`%${nombre_aeropuerto_origen}%`);
    }

    if (id_aeropuerto_destino) {
      conditions.push('apd.id=?');
      params.push(id_aeropuerto_destino);
    }

    if (codigo_aeropuerto_destino) {
      conditions.push('apd.codigo=?');
      params.push(codigo_aeropuerto_destino);
    }

    if (nombre_aeropuerto_destino) {
      conditions.push('apd.nombre like ?');
      params.push(`%${nombre_aeropuerto_destino}%`);
    }

    if (id_usuario) {
      conditions.push('u.id=?');
      params.push(id_usuario);
    }

    if (email_usuario) {
      conditions.push('u.email like ?');
      params.push(`%${email_usuario}%`);
    }

    if (numero_identificacion_usuario) {
      conditions.push('u.numero_identificacion=?');
      params.push(numero_identificacion_usuario);
    }

    if (pais_usuario) {
      conditions.push('u.pais=?');
      params.push(pais_usuario);
    }

    query = `${query} ${conditions.join(' AND ')}`;
  }

  return { query, params };
}

module.exports = {
  fullSearchFlights
};
