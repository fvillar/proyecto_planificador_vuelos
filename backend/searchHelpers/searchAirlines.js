require('dotenv').config();

function searchAirlines(queryParams) {
  let query = `SELECT * FROM aerolineas`;
  const params = [];

  const {
    codigo,
    nombre,
    direccion,
    codigo_postal,
    ciudad,
    provincia,
    pais,
    activo
  } = queryParams;

  if (
    codigo ||
    nombre ||
    direccion ||
    codigo_postal ||
    ciudad ||
    provincia ||
    pais ||
    activo
  ) {
    query = `${query} WHERE `;
    const conditions = [];

    if (codigo) {
      conditions.push('codigo like ?');
      params.push(`%${codigo}%`);
    }

    if (nombre) {
      conditions.push('nombre like ?');
      params.push(`%${nombre}%`);
    }

    if (direccion) {
      conditions.push('direccion like ?');
      params.push(`%${direccion}%`);
    }

    if (codigo_postal) {
      conditions.push('codigo_postal=?');
      params.push(codigo_postal);
    }

    if (ciudad) {
      conditions.push('ciudad like ?');
      params.push(`%${ciudad}%`);
    }

    if (provincia) {
      conditions.push('provincia like ?');
      params.push(`%${provincia}%`);
    }

    if (pais) {
      conditions.push('pais=?');
      params.push(pais);
    }

    if (activo) {
      conditions.push('activo=?');
      params.push(activo);
    }
    query = `${query} ${conditions.join(' AND ')}`;
  }

  return {
    query,
    params
  };
}

module.exports = {
  searchAirlines
};
