require('dotenv').config();

function searchTickets(queryParams) {
  let query = `SELECT 
  u.nombre AS nombre_usuario, 
  u.apellido_1 AS apellido_1_usuario, 
  u.apellido_2 AS apellido_2_usuario, 
  u.email AS email, 
  u.tipo_identificacion AS tipo_identificacion, 
  u.numero_identificacion AS numero_identidicacion, 
  apo.id AS id_aeropuerto_origen,
  apo.codigo AS codigo_aeropuerto_origen, 
  apo.nombre AS nombre_aeropuerto_origen,
  apo.ciudad AS ciudad_aeropuerto_origen, 
  apo.pais AS pais_aeropuerto_origen, 
  apd.id AS id_aeropuerto_destino,
  apd.codigo AS codigo_aeropuerto_destino, 
  apd.nombre AS nombre_aeropuerto_destino,
  apd.ciudad AS ciudad_aeropuerto_destino, 
  apd.pais AS pais_aeropuerto_destino,
  al.id AS id_aerolinea, 
  al.codigo AS codigo_aerolinea,
  al.nombre AS nombre_aerolinea, 
  v.id AS id_vuelo,
  v.codigo AS codigo_vuelo,
  v.fecha_salida AS fecha_salida, 
  v.fecha_llegada AS fecha_llegada, 
  v.hora_salida AS hora_salida, 
  v.hora_llegada AS hora_llegada, 
  v.duracion AS duracion, 
  v.numero_escalas AS numero_escalas, 
  v.precio AS precio,
  b.id AS id_billete,
  b.asiento AS asiento 
  FROM vuelos v 
  JOIN aerolineas al ON v.id_aerolinea=al.id
  JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
  JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
  JOIN billetes b ON v.id=b.id_vuelo
  JOIN usuarios u ON b.id_usuario=u.id`;

  const params = [];

  const { email_usuario, id_vuelo, codigo_vuelo, fecha_salida } = queryParams;

  if (email_usuario || id_vuelo || codigo_vuelo || fecha_salida) {
    query = `${query} WHERE `;
    const conditions = [];

    if (email_usuario) {
      conditions.push('u.email=?');
      params.push(email_usuario);
    }

    if (id_vuelo) {
      conditions.push('v.id=?');
      params.push(id_vuelo);
    }

    if (codigo_vuelo) {
      conditions.push('v.codigo=?');
      params.push(codigo_vuelo);
    }

    if (fecha_salida) {
      conditions.push('v.fecha_salida=?');
      params.push(fecha_salida);
    }

    query = `${query} ${conditions.join(' AND ')}`;
  }

  return {
    query,
    params
  };
}

module.exports = {
  searchTickets
};
