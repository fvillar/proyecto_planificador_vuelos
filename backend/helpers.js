require('dotenv').config();

const datefns = require('date-fns');
const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
const sharp = require('sharp');
const path = require('path');
const fs = require('fs-extra');
const uuid = require('uuid');

const imageUploadPath = path.join(__dirname, process.env.UPLOADS_DIR);

function generateError(message, code) {
  const error = new Error(message);
  if (code) error.httpCode = code;
  return error;
}

function randomString(size) {
  return crypto.randomBytes(size).toString('hex').slice(0, size);
}

async function sendValidateEmail({ email, title, content }) {
  sgMail.setApiKey(process.env.SENDGRID_KEY);

  const msg = {
    to: email,
    from: 'flxvllr@gmail.com',
    subject: title,
    text: content,
    html: `<div style="background-color:#acd9dd; margin-left:100px; max-width:600px">
      <h1 style="background-color: rgba(255, 255, 255, 0.5);padding:1rem; margin-bottom:0; color:#163d70; text-align:center">Bienvenido a Meuvoo. Valida tu cuenta. </h1>
      <div style="padding:1rem">${content}</div>  
    </div>`
  };

  await sgMail.send(msg);
}

async function sendRecoveryEmail({ email, title, content }) {
  sgMail.setApiKey(process.env.SENDGRID_KEY);

  const msg = {
    to: email,
    from: 'flxvllr@gmail.com',
    subject: title,
    text: content,
    html: `<div style="background-color:#acd9dd; margin-left:100px; max-width:600px">
      <h1 style="background-color: rgba(255, 255, 255, 0.5);padding:1rem; margin-bottom:0; color:#163d70; text-align:center">Recuperacion de contraseña Meuvoo. </h1>
      <div style="padding:1rem">${content}</div>  
    </div>`
  };

  await sgMail.send(msg);
}

async function sendTicketEmail({ email, title, content }) {
  sgMail.setApiKey(process.env.SENDGRID_KEY);

  const msg = {
    to: email,
    from: 'flxvllr@gmail.com',
    subject: title,
    text: content,
    html: `<div style="background-color:#acd9dd; margin-left:100px; max-width:600px">
    <h1 style="background-color: rgba(255, 255, 255, 0.5);padding:1rem; margin-bottom:0; color:#163d70; text-align:center">Gracias por su compra en  Meuvoo.</h1>
    <h2 style="background-color: rgba(255, 255, 255, 0.5);padding:1rem; margin-top:0; color:#1cb36f; text-align:center">Elige tus vuelos</h2>
      <div>${content}</div>  
      <h2 style="background-color: rgba(255, 255, 255, 0.5);padding:1rem; color:#1cb36f;text-align:center">Descubre tus sueños</h2>
    </div>`
  };

  await sgMail.send(msg);
}

async function processAndSavePhoto(uploadedImage) {
  // Damos un nombre aleatorio y unico a la foto
  const savedFileName = `${uuid.v1()}.jpg`;

  // Nos aseguramos de que existe la ruta
  await fs.ensureDir(imageUploadPath);

  // Procesamos la imagen
  const finalImage = sharp(uploadedImage.data);

  // Comprobamos el tamaño
  const imageInfo = await finalImage.metadata();

  // Si el ancho de la imagen es superior a 400, la modificamos
  if (imageInfo.width > 400) {
    finalImage.resize(400);
  }

  // Salvamos la imagen
  await finalImage.toFile(path.join(imageUploadPath, savedFileName));

  return savedFileName;
}

async function deletePhoto(imagePath) {
  if (imagePath !== 'perfilpordefecto.png') {
    await fs.unlink(path.join(imageUploadPath, imagePath));
  }
}

function calculateFlightDuration(arrData, arrHour, depData, depHour) {
  const duration = datefns.differenceInMinutes(
    new Date(`${arrData} ${arrHour}`),
    new Date(`${depData} ${depHour}`)
  );
  return duration;
}

function formatFlightDuration(duration) {
  let hours = Math.floor(duration / 60);
  let minutes = Math.floor(duration % 60);
  const h = hours < 10 ? '0' + hours : hours;
  const m = minutes < 10 ? '0' + minutes : minutes;

  return `${h}h${m}m`;
}

function formatDBDateToHumanFormat(fecha) {
  return new Date(
    fecha.getFullYear(),
    fecha.getMonth(),
    fecha.getDate()
  ).toDateString();
}

function formatBirthdateDate(fecha) {
  return datefns.format(fecha, 'yyyy-MM-dd');
}

function showFligthDataHumanFormat(flightData) {
  const showFlightData = flightData.map((vuelo) => {
    vuelo.fecha_salida = formatDBDateToHumanFormat(vuelo.fecha_salida);
    vuelo.fecha_llegada = formatDBDateToHumanFormat(vuelo.fecha_llegada);
    vuelo.duracion = formatFlightDuration(vuelo.duracion);
    return vuelo;
  });
  return showFlightData;
}

module.exports = {
  generateError,
  randomString,
  sendValidateEmail,
  sendRecoveryEmail,
  sendTicketEmail,
  processAndSavePhoto,
  deletePhoto,
  calculateFlightDuration,
  showFligthDataHumanFormat,
  formatBirthdateDate
};
