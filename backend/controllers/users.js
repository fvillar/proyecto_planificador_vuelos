require('dotenv').config();

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { getConnection } = require('../db');
const {
  newUserSchema,
  userLoginSchema,
  editUserSchema,
  editPasswordUserSchema,
  enableUserSchema,
  recoveryPasswordSchema
} = require('./validations/userValidations');

const {
  generateError,
  randomString,
  sendValidateEmail,
  sendRecoveryEmail,
  processAndSavePhoto,
  deletePhoto,
  formatBirthdateDate
} = require('../helpers');

const { searchUsers } = require('../searchHelpers/searchUsers');

async function newUser(req, res, next) {
  let connection;
  try {
    //Establecemos conecxion con la base de datos
    connection = await getConnection();
    //Validamos los datos introducidos por el usuario
    await newUserSchema.validateAsync(req.body);
    //Destructuramos los datos que nos vienen en el body
    const { email, password } = req.body;
    //Comprobamos si ya existe el usuario en nuestra base de datos
    const [
      existingEmail
    ] = await connection.query('SELECT id from usuarios where email=?', [
      email
    ]);

    if (existingEmail.length) {
      throw generateError(
        'Este email ya está registrado en la base de datos',
        409
      );
    }
    //Ciframos la contraseña que introduce el usuario
    const dbPassword = await bcrypt.hash(password, 10);
    //Creamos codigo de registro y montamos url que enviaremos al usuario
    const registrationCode = randomString(40);
    const validationURL = `${process.env.PUBLIC_HOST}/users/validate?code=${registrationCode}`;
    //Enviamos email de registro al usuario
    try {
      await sendValidateEmail({
        email: email,
        title: 'Valida tu cuenta',
        content: `<p style="color:#163d70; text-align:center;font-size:1rem">Para validar tu cuenta pega esta URL en tu navegador:</p> 
        <p style="text-align:center;font-size:.8rem"> <a href="${validationURL}" style="text-decoration:none; color:#1cb36f">${validationURL}</a></p>`
      });
    } catch (error) {
      console.error(error);
      throw generateError(
        'Se ha producido un error en el envío del mail. Prueba mas tarde',
        401
      );
    }

    await connection.query(
      `INSERT INTO usuarios ( email, password, activo, rol, codigo_registro)
      VALUES (?,?,0,"usuario",?) `,
      [email, dbPassword, registrationCode]
    );

    res.send({
      staus: 'ok',
      message:
        'Usuario introducido en la base de datos. Verificalo desde tu correo electrónico. No olvides revisar tu carpeta de spam.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function validateUser(req, res, next) {
  let connection;

  try {
    const { code } = req.query;

    connection = await getConnection();

    // Lanzamos consulta de actualizacion
    const [
      result
    ] = await connection.query(
      'UPDATE usuarios SET activo=1,codigo_registro=NULL WHERE codigo_registro=?',
      [code]
    );

    if (result.affectedRows === 0) {
      throw generateError('Validación incorrecta', 400);
    }
    res.send(
      /* {
      staus: 'ok',
      message:
        'Usuario validado, ya puedes hacer login con tu email y tu contraseña.'
    } */
      `<div style="width:100vw; height:100vh; background-color:#acd9dd">
      <h1 style="font-family: Poppins, sans-serif; background-color: rgba(255, 255, 255, 0.5);padding:2rem 1rem;  color:#163d70; text-align:center;font-size:3rem">Bienvenido a Meuvoo</h1>
      <h3 style=" font-family: Poppins, sans-serif; color:#163d70; text-align:center; margin:5rem auto 0 auto; padding:1rem; font-size:2rem">Has validado tu cuenta de usuario</h3>
      <p style="font-family: Poppins, sans-serif; color:#163d70; text-align:center; margin:0 auto;padding:1rem;font-size:1.5rem">¿Te apetece loguearte?</p>
      <p style="font-family: Poppins, sans-serif; color:#163d70; text-align:center; margin:0 auto;padding:1rem;font-size:1.5rem"><a style="text-decoration:none; color:#1cb36f" href="http://localhost:8080/#/login">Pulsa aquí, elige y descubre...</a></p>
      </div>`
    );
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function loginUser(req, res, next) {
  let connection;

  try {
    await userLoginSchema.validateAsync(req.body);

    const { email, password } = req.body;

    connection = await getConnection();

    // Buscamos el usuario en la base de datos
    const [
      dbUser
    ] = await connection.query(
      'SELECT id, email, password, rol from usuarios where email=? AND activo=1',
      [email]
    );

    if (!dbUser.length) {
      throw generateError(
        'No hay ningún usuario activo con ese email en la base de datos. Si te acabas de registrar recuerda ir a tu correo para validar tu cuenta. Si has deshabilitado tu cuenta, actívala de nuevo',
        404
      );
    }

    const [user] = dbUser;

    const passwordsMatch = await bcrypt.compare(password, user.password);

    if (!passwordsMatch) {
      throw generateError('Contraseña incorrecta', 401);
    }

    // Construimos el token con jsonwebtoken
    const tokenPayload = {
      id: user.id,
      rol: user.rol,
      email: user.email
    };
    const token = jwt.sign(tokenPayload, process.env.SECRET, {
      expiresIn: '2d'
    });

    // Creamos respuesta
    res.send({
      status: 'ok',
      message: 'Login correcto',
      data: { token }
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function basicInfoUser(req, res, next) {
  let connection;

  try {
    const { id } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
      SELECT id, email, rol, fecha_creacion, fecha_modificacion 
      FROM usuarios WHERE id=?`,
      [id]
    );

    // Configuramos error en caso de que no exista usuario
    if (!result.length) {
      throw generateError(`No existe ningún usuario con el id ${id}`, 404);
    }

    //Obtenemos datos del usuario
    const [userData] = result;

    const payload = {
      id: userData.id
    };

    if (userData.id === req.auth.id || req.auth.rol === 'administrador') {
      payload.email = userData.email;
      payload.rol = userData.rol;
      payload.fecha_registro = userData.fecha_creacion;
      payload.fecha_modificacion = userData.fecha_modificacion;
    }

    res.send({
      status: 'ok',
      data: payload
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function allInfoUser(req, res, next) {
  let connection;

  try {
    const { id } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
      SELECT id, email, rol, activo,nombre, apellido_1, apellido_2, tipo_identificacion, numero_identificacion, direccion, codigo_postal, ciudad, provincia, pais, fecha_nacimiento, telefono, foto, numero_vuelos, fecha_creacion, fecha_modificacion
      FROM usuarios WHERE id=?  `,
      [id]
    );

    // Configuramos error por si no hay resultados a la consulta
    if (!result.length) {
      throw generateError(`No existe el usuario con id ${id}`, 404);
    }

    const [userData] = result;

    const payload = {
      id: userData.id
    };

    if (userData.id === req.auth.id || req.auth.rol === 'administrador') {
      payload.email = userData.email;
      payload.rol = userData.rol;
      payload.activo = userData.activo;
      payload.nombre = userData.nombre;
      payload.apellido_1 = userData.apellido_1;
      payload.apellido_2 = userData.apellido_2;
      payload.tipo_identificacion = userData.tipo_identificacion;
      payload.numero_identificacion = userData.numero_identificacion;
      payload.direccion = userData.direccion;
      payload.codigo_postal = userData.codigo_postal;
      payload.ciudad = userData.ciudad;
      payload.provincia = userData.provincia;
      payload.pais = userData.pais;
      if (!!userData.fecha_nacimiento) {
        payload.fecha_nacimiento = formatBirthdateDate(
          userData.fecha_nacimiento
        );
      } else {
        payload.fecha_nacimiento = userData.fecha_nacimiento;
      }
      payload.telefono = userData.telefono;
      payload.foto = userData.foto;
      payload.fecha_creacion = userData.fecha_creacion;
      payload.fecha_modificacion = userData.fecha_modificacion;
    }

    res.send({
      status: 'ok',
      data: payload
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getAllUsers(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { query, params } = searchUsers(req.query);
    console.log(query, params);
    const result = await connection.query(query, params);

    const [usersData] = result;

    if (!usersData.length) {
      throw generateError('La lista de usuarios está vacia', 401);
    }

    const usersFinalData = usersData.map((usuario) => {
      if (!!usuario.fecha_nacimiento) {
        usuario.fecha_nacimiento = formatBirthdateDate(
          usuario.fecha_nacimiento
        );
      }
      return usuario;
    });

    res.send({
      staus: 'ok',
      data: usersFinalData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function updatePasswordUser(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    // Comprobamos que el usuario del token es el mismo que el que vamos a cambiar la pass
    if (Number(id) !== req.auth.id) {
      throw generateError(
        `No tienes permisos para cambiar la password del usuario con id ${id}`,
        401
      );
    }

    connection = await getConnection();

    await editPasswordUserSchema.validateAsync(req.body);

    const { oldPassword, newPassword } = req.body;

    if (oldPassword === newPassword) {
      throw generateError(
        'La nueva password no puede ser la misma que la antigua',
        400
      );
    }

    // Sacar la info del usuario de la base de datos
    const [
      currentUser
    ] = await connection.query(`SELECT id, password from usuarios where id=?`, [
      id
    ]);

    const [dbUser] = currentUser;

    // Comprobamos que la vieja password es la correcta
    const passwordsMatch = await bcrypt.compare(oldPassword, dbUser.password);

    if (!passwordsMatch) {
      throw generateError('Tu password antigua es incorrecta', 401);
    }

    // Encriptamos la nueva contraseña con bcrypt
    const dbNewPassword = await bcrypt.hash(newPassword, 10);

    // Actualizamos la base de datos
    await connection.query(
      `
      UPDATE usuarios SET password=?, fecha_cambio_password=NOW() WHERE id=?
    `,
      [dbNewPassword, id]
    );

    res.send({
      status: 'ok',
      message:
        'Cambio de password realizado correctamente. Todos tus tokens dejan de ser validos. Vuelve a loguearte.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function editUser(req, res, next) {
  let connection;

  try {
    await editUserSchema.validateAsync(req.body);

    const { id } = req.params;
    const {
      nombre,
      apellido_1,
      apellido_2,
      tipo_identificacion,
      numero_identificacion,
      direccion,
      codigo_postal,
      ciudad,
      provincia,
      pais,
      fecha_nacimiento,
      telefono
    } = req.body;

    connection = await getConnection();

    // Comprobamos si existe el usuario
    const [current] = await connection.query(
      `
      SELECT 
        id,
        foto
      FROM usuarios 
      WHERE id=?`,
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe el usuario con id ${id} `, 404);
    }

    const [dbUser] = current;
    // Comprobamos si quien va a modificar, es el mismo usuario que el validado o si es un administrador
    if (dbUser.id !== req.auth.id && req.auth.rol !== 'administrador') {
      throw generateError('No tienes permisos para editar este usuario', 401);
    }

    // Comprobamos si recibimos una foto y la procesamos
    let savedFileName;
    if (req.files && req.files.foto) {
      try {
        savedFileName = await processAndSavePhoto(req.files.foto);
        if (current && dbUser.foto) {
          await deletePhoto(dbUser.foto);
        }
      } catch (error) {
        throw generateError(
          'No se puede procesar la imagen. Intentelo de nuevo más tarde.',
          400
        );
      }
    } else {
      savedFileName = dbUser.foto;
    }

    // Actualizamos el usuario
    await connection.query(
      `UPDATE usuarios 
      SET 
        nombre=?,
        apellido_1=?,
        apellido_2=?,
        tipo_identificacion=?,
        numero_identificacion=?,
        direccion=?,
        codigo_postal=?,
        ciudad=?,
        provincia=?,
        pais=?,
        fecha_nacimiento=?,
        telefono=?,
        foto=?
      WHERE id=?`,
      [
        nombre,
        apellido_1,
        apellido_2,
        tipo_identificacion,
        numero_identificacion,
        direccion,
        codigo_postal,
        ciudad,
        provincia,
        pais,
        fecha_nacimiento,
        telefono,
        savedFileName,
        id
      ]
    );

    res.send({ status: 'ok', message: 'Usuario actualizado correctamente' });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function deleteUser(req, res, next) {
  let connection;
  try {
    const { id } = req.params;

    connection = await getConnection();

    //Comprobamos si el usuario logueado es administrador
    if (!req.auth || req.auth.rol !== 'administrador') {
      throw generateError(
        'Solo un administrador puede eliminar un usuario',
        400
      );
    }

    //Comprobamos si existe el usuario y eliminamos la foto si existe
    const [
      current
    ] = await connection.query('SELECT foto FROM usuarios WHERE id=?', [id]);

    if (!current.length) {
      throw generateError(`No existe el usuario con id ${id}`, 400);
    }

    const [dbUser] = current;

    if (dbUser.foto) {
      await deletePhoto(dbUser.foto);
    }

    //Eliminamos el usuario de la base de datos
    await connection.query('DELETE FROM usuarios WHERE id=?', [id]);

    res.send({
      status: 'ok',
      message: `Usuario borrado correctamente`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function disableUser(req, res, next) {
  let connection;
  try {
    const { id } = req.params;

    connection = await getConnection();

    const [
      current
    ] = await connection.query('SELECT id, activo from usuarios where id=?', [
      id
    ]);

    if (!current.length) {
      throw generateError(
        `No existe el usuario con id ${id} en la base de datos`,
        400
      );
    }

    const [dbUser] = current;

    if (dbUser.id !== req.auth.id && req.auth.rol !== 'administrador') {
      throw generateError(
        'No tienes permisos para deshabilitar este usuario',
        401
      );
    }

    await connection.query(
      'UPDATE usuarios SET activo=0, fecha_cambio_password=NOW() WHERE id=?',
      [id]
    );

    res.send({
      status: 'ok',
      message: `El usuario ha sido deshabilitado`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function enableUser(req, res, next) {
  let connection;

  try {
    await enableUserSchema.validateAsync(req.body);

    const { email, password } = req.body;

    connection = await getConnection();

    // Buscamos el usuario en la base de datos
    const [
      current
    ] = await connection.query(
      'SELECT id, email, password from usuarios where email=? AND activo=0 AND codigo_registro IS null',
      [email]
    );

    if (!current.length) {
      throw generateError(
        'No existe ninguna cuenta desactivada con ese email',
        404
      );
    }

    const [dbUser] = current;

    const passwordsMatch = await bcrypt.compare(password, dbUser.password);

    if (!passwordsMatch) {
      throw generateError('La contraseña introducida es incorrecta', 401);
    }

    await connection.query(
      `UPDATE usuarios SET activo=1 WHERE id=?
    `,
      [dbUser.id]
    );

    // Creamos respuesta
    res.send({
      status: 'ok',
      message:
        'La reactivacion de la cuenta se ha realizado. Vuelve a loguearte para iniciar sesion'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function recoveryPassword(req, res, next) {
  let connection;

  try {
    await recoveryPasswordSchema.validateAsync(req.body);

    const { email } = req.body;

    connection = await getConnection();

    // Buscamos el usuario en la base de datos
    const [
      current
    ] = await connection.query(
      'SELECT id, email, password from usuarios where email=? and activo=1',
      [email]
    );

    if (!current.length) {
      throw generateError(
        'No exite ningun usuario activo con ese email en la base de datos',
        404
      );
    }

    const [dbUser] = current;

    //Creamos password temporal para el usuario.
    const tempPassword = randomString(10);

    //Enviamos email con nueva contraseña al usuario
    try {
      await sendRecoveryEmail({
        email: dbUser.email,
        title: 'Esta es tu nueva contraseña',
        content: `<p style="color:#1cb36f; text-align:center; font-size:1rem">Tu nueva contraseña es ${tempPassword}.</p>
        <p style="color:#163d70; text-align:center;font-size:1rem"> Por tu seguridad, te recomendamos que la cambies en tu primer acceso a la página.</p>`
      });
    } catch (error) {
      console.error(error);
      throw generateError(
        'Se ha producido un error en el envío del mail. Prueba mas tarde',
        401
      );
    }

    //Ciframos la nueva contraseña
    const tempDBPassword = await bcrypt.hash(tempPassword, 10);
    //Actualizamos la base de datos con la nueva contraseña cifrada
    await connection.query('UPDATE usuarios SET password=? where id=?', [
      tempDBPassword,
      dbUser.id
    ]);

    res.send({
      staus: 'ok',
      message:
        'Recuperacion de contraseña completada. Te hemos enviado un email con tu nueva contraseña. No olvides revisar tu carpeta de SPAM.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  newUser,
  validateUser,
  loginUser,
  basicInfoUser,
  allInfoUser,
  getAllUsers,
  updatePasswordUser,
  editUser,
  deleteUser,
  disableUser,
  enableUser,
  recoveryPassword
};
