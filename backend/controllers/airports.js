require('dotenv').config();

const { generateError } = require('../helpers');
const { getConnection } = require('../db');

const {
  airportSchema,
  modifyAirportSchema
} = require('./validations/airportValidations');

const { searchAirports } = require('../searchHelpers/searchAirports');

async function newAirport(req, res, next) {
  let connection;

  try {
    //Establecemos conecxion con la base de datos
    connection = await getConnection();

    //Validamos los datos introducidos por el usuario
    await airportSchema.validateAsync(req.body);

    //Destructuramos los datos que nos vienen en el body
    const {
      codigo,
      nombre,
      direccion,
      codigo_postal,
      ciudad,
      provincia,
      pais
    } = req.body;

    //Comprobamos si ya existe la aerolinea en nuestra base de datos
    const [
      existingAirport
    ] = await connection.query('SELECT id from aeropuertos where codigo=?', [
      codigo
    ]);

    if (existingAirport.length) {
      throw generateError('Este aeropuerto ya existe en la base de datos', 409);
    }

    await connection.query(
      `INSERT INTO aeropuertos ( codigo, nombre,direccion, codigo_postal, ciudad, provincia, pais )
      VALUES (?,?,?,?,?,?,?)`,
      [codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais]
    );

    res.send({
      staus: 'ok',
      message: 'Aeropuerto incorporado a la base de datos'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function getAllAirports(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { query, params } = searchAirports(req.query);
    const result = await connection.query(query, params);

    const [airportData] = result;

    if (!airportData.length) {
      throw generateError('La lista de aerolineas está vacia', 401);
    }

    res.send({
      staus: 'ok',
      data: airportData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getAirport(req, res, next) {
  let connection;

  try {
    const { id } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
      SELECT id, nombre, direccion, codigo_postal, ciudad, provincia, pais, activo, fecha_creacion, fecha_modificacion
      FROM aeropuertos WHERE id=?  `,
      [id]
    );

    // Configuramos error por si no hay resultados a la consulta
    if (!result.length) {
      throw generateError(`No existe el aeropuerto con id ${id}`, 404);
    }

    const [airportData] = result;

    const payload = {
      id: airportData.id,
      codigo: airportData.codigo,
      nombre: airportData.nombre,
      direccion: airportData.direccion,
      codigo_postal: airportData.codigo_postal,
      ciudad: airportData.ciudad,
      provincia: airportData.provincia,
      pais: airportData.pais,
      activo: airportData.activo,
      creacion: airportData.fecha_creacion,
      modificacion: airportData.fecha_modificacion
    };

    res.send({
      status: 'ok',
      data: payload
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function editAirport(req, res, next) {
  let connection;

  try {
    await airportSchema.validateAsync(req.body);

    const { id } = req.params;

    const {
      codigo,
      nombre,
      direccion,
      codigo_postal,
      ciudad,
      provincia,
      pais
    } = req.body;

    connection = await getConnection();

    // Comprobamos si existe el id y la aerolinea
    const [current] = await connection.query(
      `
      SELECT 
      id, codigo
      FROM aeropuertos 
      WHERE id=?`,
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe ese aeropuerto.`, 404);
    }

    // Actualizamos el aeropuerto
    await connection.query(
      `UPDATE aeropuertos 
        SET 
        codigo=?,
        nombre=?,
        direccion=?,
        codigo_postal=?,
        ciudad=?,
        provincia=?,
        pais=?
        WHERE id=?`,
      [codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais, id]
    );

    res.send({ status: 'ok', message: 'Aeropuerto actualizado' });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function deleteAirport(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    // Comprobamos si existe el aeropuerto
    const [current] = await connection.query(
      `
      SELECT 
      id, codigo
      FROM aeropuertos 
      WHERE id=?`,
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe el aeropuerto.`, 404);
    }

    await connection.query('DELETE FROM aeropuertos WHERE id=?', [id]);

    res.send({
      status: 'ok',
      message: `El aeropuerto ha sido eliminado de la base de datos`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function disableAirport(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    const [
      current
    ] = await connection.query(
      'SELECT id, codigo, activo from aeropuertos where id=? AND activo=1',
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe un aeropuerto activo con esos datos`, 400);
    }

    await connection.query('UPDATE aeropuertos SET activo=0 WHERE id=?', [id]);

    res.send({
      status: 'ok',
      message: `El aeropuerto ha sido deshabilitado correctamente`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function enableAirport(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    // Buscamos el aeropuerto en la base de datos
    const [
      current
    ] = await connection.query(
      'SELECT id, codigo, activo from aeropuertos where id=? AND activo=0',
      [id]
    );

    if (!current.length) {
      throw generateError(
        `No existe ningun aeropuerto desactivado con esos datos`,
        404
      );
    }

    await connection.query(
      `UPDATE aeropuertos SET activo=1 WHERE id=?
    `,
      [id]
    );

    // Creamos respuesta
    res.send({
      status: 'ok',
      message: `El aeropuerto ha sido habilitado correctamente`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getAirportCities(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const result = await connection.query(
      'SELECT DISTINCT ciudad from aeropuertos ORDER BY ciudad ASC'
    );

    const [citiesData] = result;

    if (!citiesData.length) {
      throw generateError(
        'La lista de ciudades de aeropuertos está vacia',
        401
      );
    }

    res.send({
      staus: 'ok',
      data: citiesData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = {
  newAirport,
  getAllAirports,
  getAirport,
  editAirport,
  deleteAirport,
  disableAirport,
  enableAirport,
  getAirportCities
};
