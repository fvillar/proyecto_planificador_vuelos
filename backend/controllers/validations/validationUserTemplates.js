const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const emailSchema = Joi.string()
  .email()
  .required()
  .error(generateError('La direccion de email introducida no es valida', 400));

const passwordSchema = Joi.string()
  .min(6)
  .alphanum()
  .max(50)
  .required()
  .error(
    generateError('La contraseña debe tener entre 6 y 50 caracteres', 400)
  );

const nombreSchema = Joi.string()
  .min(2)
  .max(50)
  .error(generateError('El nombre debe tener entre 2 y 50 caracteres', 400));

const apellido_1Schema = Joi.string()
  .min(2)
  .max(50)
  .error(
    generateError('El primer apellido debe tener entre 2 y 50 caracteres', 400)
  );

const apellido_2Schema = Joi.string()
  .min(2)
  .max(50)
  .error(
    generateError('El segundo apellido debe tener entre 2 y 50 caracteres', 400)
  );

const tipo_identificacionSchema = Joi.string()
  .min(2)
  .max(20)
  .error(
    generateError(
      'El tipo de identificacion debe tener entre 2 y 20 caracteres',
      400
    )
  );

const numero_identificacionSchema = Joi.string()
  .min(2)
  .max(30)
  .error(
    generateError(
      'El numero de identificacion debe tener entre 2 y 20 caracteres',
      400
    )
  );

const direccionSchema = Joi.string()
  .min(4)
  .max(150)
  .error(
    generateError('La direcion debe tener entre 10 y 150 caracteres', 400)
  );

const codigo_postalSchema = Joi.string()
  .min(2)
  .max(20)
  .error(
    generateError('El codigo postal debe tener entre 2 y 10 caracteres', 400)
  );

const ciudadSchema = Joi.string()
  .min(2)
  .max(100)
  .error(
    generateError('La localidad debe tener entre 2 y 100 caracteres', 400)
  );

const provinciaSchema = Joi.string()
  .min(2)
  .max(100)
  .error(generateError('La ciudad debe tener entre 2 y 50 caracteres', 400));

const paisSchema = Joi.string()
  .min(2)
  .max(50)
  .error(generateError('El pais debe tener entre 2 y 50 caracteres', 400));

const fecha_nacimientoSchema = Joi.date()
  .format('YYYY-MM-DD')
  .utc()
  .error(
    generateError('La fecha es erronea. Debe tener format YYYY-MM-DD', 400)
  );

const telefonoSchema = Joi.string()
  .min(2)
  .max(50)
  .error(
    generateError('El numero de telefono debe tener entre 2 y 50 numeros', 400)
  );

const rolSchema = Joi.any()
  .valid('administrador', 'usuario', 'pasajero')
  .error(generateError(`El rol que intentas asignar no es correcto`));

const activoSchema = Joi.number()
  .integer()
  .valid(0, 1)
  .error(generateError(`El valor del campo activo debe ser 0 o 1`));

const codigo_registroSchema = Joi.string()
  .length(40)
  .hex()
  .error(generateError(`Codigo de registro incorrecto`));

module.exports = {
  emailSchema,
  passwordSchema,
  nombreSchema,
  apellido_1Schema,
  apellido_2Schema,
  tipo_identificacionSchema,
  numero_identificacionSchema,
  direccionSchema,
  codigo_postalSchema,
  ciudadSchema,
  provinciaSchema,
  paisSchema,
  fecha_nacimientoSchema,
  telefonoSchema,
  rolSchema,
  activoSchema,
  codigo_registroSchema
};
