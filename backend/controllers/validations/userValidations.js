const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const {
  emailSchema,
  passwordSchema,
  nombreSchema,
  apellido_1Schema,
  apellido_2Schema,
  tipo_identificacionSchema,
  numero_identificacionSchema,
  direccionSchema,
  codigo_postalSchema,
  ciudadSchema,
  provinciaSchema,
  paisSchema,
  fecha_nacimientoSchema,
  telefonoSchema
} = require('./validationUserTemplates.js');

const newUserSchema = Joi.object().keys({
  email: emailSchema,
  password: passwordSchema
});

const userLoginSchema = Joi.object().keys({
  email: emailSchema,
  password: passwordSchema
});

const editUserSchema = Joi.object().keys({
  nombre: nombreSchema,
  apellido_1: apellido_1Schema,
  apellido_2: apellido_2Schema,
  tipo_identificacion: tipo_identificacionSchema,
  numero_identificacion: numero_identificacionSchema,
  direccion: direccionSchema,
  codigo_postal: codigo_postalSchema,
  ciudad: ciudadSchema,
  provincia: provinciaSchema,
  pais: paisSchema,
  fecha_nacimiento: fecha_nacimientoSchema,
  telefono: telefonoSchema
});

const editPasswordUserSchema = Joi.object().keys({
  oldPassword: passwordSchema,
  newPassword: passwordSchema,
  newPasswordRepeat: Joi.any()
    .valid(Joi.ref('newPassword'))
    .error(generateError('Las passwords debe ser iguales', 400))
});

const enableUserSchema = Joi.object().keys({
  email: emailSchema,
  password: passwordSchema
});

const recoveryPasswordSchema = Joi.object().keys({
  email: emailSchema
});

module.exports = {
  newUserSchema,
  userLoginSchema,
  editUserSchema,
  editPasswordUserSchema,
  enableUserSchema,
  recoveryPasswordSchema
};
