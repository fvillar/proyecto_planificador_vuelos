const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const codigoAirlineSchema = Joi.string()
  .uppercase()
  .length(3)
  .required()
  .error(
    generateError('El codigo de la aerolinea debe tener 3 caracteres', 400)
  );

const nombreAirlineSchema = Joi.string()
  .min(2)
  .max(150)
  .required()
  .error(
    generateError(
      'El nombre de la aerolinea debe tener entre 2 y 150 caracteres',
      400
    )
  );

const direccionAirlineSchema = Joi.string()
  .min(10)
  .max(150)
  .error(
    generateError(
      'La direcion de la aerolinea debe tener entre 10 y 150 caracteres',
      400
    )
  );

const codigo_postalAirlineSchema = Joi.string()
  .min(2)
  .max(20)
  .error(
    generateError(
      'El codigo postal de la aerolinea debe tener entre 2 y 10 caracteres',
      400
    )
  );

const ciudadAirlineSchema = Joi.string()
  .min(2)
  .max(100)
  .error(
    generateError(
      'La localidad de la aerolinea debe tener entre 2 y 100 caracteres',
      400
    )
  );

const provinciaAirlineSchema = Joi.string()
  .min(2)
  .max(100)
  .error(
    generateError(
      'La ciudad de la aerolinea debe tener entre 2 y 50 caracteres',
      400
    )
  );

const paisAirlineSchema = Joi.string()
  .min(2)
  .max(50)
  .required()
  .error(
    generateError(
      'El pais de la aerolinea debe tener entre 2 y 50 caracteres',
      400
    )
  );

module.exports = {
  codigoAirlineSchema,
  nombreAirlineSchema,
  direccionAirlineSchema,
  codigo_postalAirlineSchema,
  ciudadAirlineSchema,
  provinciaAirlineSchema,
  paisAirlineSchema
};
