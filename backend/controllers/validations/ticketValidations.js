const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const {
  asientoTicketSchema,
  idTicketSchema
} = require('./validationTicketTemplates');

const newTicketSchema = Joi.object().keys({
  id_vuelo: idTicketSchema,
  asiento: asientoTicketSchema
});

const editTicketSchema = Joi.object().keys({
  id_usuario: idTicketSchema,
  id_usuario_nuevo: idTicketSchema,
  id_vuelo: idTicketSchema
});

const deleteTicketSchema = Joi.object().keys({
  id_usuario: idTicketSchema,
  id_vuelo: idTicketSchema
});

module.exports = {
  newTicketSchema,
  editTicketSchema,
  deleteTicketSchema
};
