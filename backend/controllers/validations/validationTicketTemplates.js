const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const asientoTicketSchema = Joi.string()
  .min(2)
  .max(6)
  .required()
  .error(
    generateError('El numero de asiento debe tener entre 2 y 6 caracteres', 400)
  );

const idTicketSchema = Joi.number()
  .integer()
  .positive()
  .error(generateError('El id introducido es incorrecto', 400));

const emailTicketSchema = Joi.string()
  .email()
  .error(generateError('La direccion de email introducida no es valida', 400));

module.exports = {
  asientoTicketSchema,
  idTicketSchema,
  emailTicketSchema
};
