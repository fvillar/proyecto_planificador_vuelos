const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const {
  codigoAirportSchema,
  nombreAirportSchema,
  direccionAirportSchema,
  codigo_postalAirportSchema,
  ciudadAirportSchema,
  provinciaAirportSchema,
  paisAirportSchema
} = require('./validationAirportTemplates');

const airportSchema = Joi.object().keys({
  codigo: codigoAirportSchema,
  nombre: nombreAirportSchema,
  direccion: direccionAirportSchema,
  codigo_postal: codigo_postalAirportSchema,
  ciudad: ciudadAirportSchema,
  provincia: provinciaAirportSchema,
  pais: paisAirportSchema
});

const modifyAirportSchema = Joi.object().keys({
  codigo: codigoAirportSchema
});

module.exports = {
  airportSchema,
  modifyAirportSchema
};
