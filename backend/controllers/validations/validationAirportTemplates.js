const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const codigoAirportSchema = Joi.string()
  .uppercase()
  .length(3)
  .required()
  .error(
    generateError('El codigo del aeropuerto debe tener 3 caracteres', 400)
  );

const nombreAirportSchema = Joi.string()
  .min(2)
  .max(150)
  .required()
  .error(
    generateError(
      'El nombre del aeropuerto debe tener entre 2 y 150 caracteres',
      400
    )
  );

const direccionAirportSchema = Joi.string()
  .min(10)
  .max(150)
  .error(
    generateError(
      'La direcion del aeropuerto debe tener entre 10 y 150 caracteres',
      400
    )
  );

const codigo_postalAirportSchema = Joi.string()
  .min(2)
  .max(20)
  .error(
    generateError(
      'El codigo postal del aeropuerto debe tener entre 2 y 10 caracteres',
      400
    )
  );

const ciudadAirportSchema = Joi.string()
  .min(2)
  .max(100)
  .required()
  .error(
    generateError(
      'La ciudad del aeropuerto debe tener entre 2 y 100 caracteres',
      400
    )
  );

const provinciaAirportSchema = Joi.string()
  .min(2)
  .max(100)
  .error(
    generateError(
      'La ciudad del aeropuerto debe tener entre 2 y 100 caracteres',
      400
    )
  );

const paisAirportSchema = Joi.string()
  .min(2)
  .max(50)
  .required()
  .error(
    generateError(
      'El pais del aeropuerto debe tener entre 2 y 50 caracteres',
      400
    )
  );

module.exports = {
  codigoAirportSchema,
  nombreAirportSchema,
  direccionAirportSchema,
  codigo_postalAirportSchema,
  ciudadAirportSchema,
  provinciaAirportSchema,
  paisAirportSchema
};
