const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const {
  codigoFlightSchema,
  fecha_FlightSchema,
  hora_FlightSchema,
  escalasFlightSchema,
  precioFlightSchema
} = require('./validationFlightTemplates.js');

const { codigoAirlineSchema } = require('./validationAirlineTemplates.js');

const { codigoAirportSchema } = require('./validationAirportTemplates.js');

const newFlightSchema = Joi.object().keys({
  codigo: codigoFlightSchema,
  codigo_aerolinea: codigoAirlineSchema,
  codigo_aeropuerto_origen: codigoAirportSchema,
  codigo_aeropuerto_destino: codigoAirportSchema,
  fecha_salida: fecha_FlightSchema,
  fecha_llegada: fecha_FlightSchema,
  hora_salida: hora_FlightSchema,
  hora_llegada: hora_FlightSchema,
  escalas: escalasFlightSchema,
  precio: precioFlightSchema
});

const getFlightsAirlineSchema = Joi.object().keys({
  codigo: codigoAirlineSchema
});

const getFlightsAirportSchema = Joi.object().keys({
  codigo: codigoAirportSchema
});

const editFlightSchema = Joi.object().keys({
  codigo: codigoFlightSchema,
  codigo_aerolinea: codigoAirlineSchema,
  codigo_aeropuerto_origen: codigoAirportSchema,
  codigo_aeropuerto_destino: codigoAirportSchema,
  fecha_salida: fecha_FlightSchema,
  fecha_llegada: fecha_FlightSchema,
  hora_salida: hora_FlightSchema,
  hora_llegada: hora_FlightSchema,
  escalas: escalasFlightSchema,
  precio: precioFlightSchema
});

module.exports = {
  newFlightSchema,
  getFlightsAirlineSchema,
  getFlightsAirportSchema,
  editFlightSchema
};
