const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const { generateError } = require('../../helpers');

const codigoFlightSchema = Joi.string()
  .min(5)
  .max(10)
  .required()
  .error(
    generateError('El codigo del vuelo debe tener entre 5 y 10 caracteres', 400)
  );

const fecha_FlightSchema = Joi.date()
  .format('YYYY-MM-DD')
  .utc()
  .error(
    generateError('La fecha es erronea. Debe tener formato YYYY-MM-DD', 400)
  );

const hora_FlightSchema = Joi.string()
  .length(5)
  .required()
  .error(generateError('La hora es erronea', 400));

const escalasFlightSchema = Joi.string()
  .min(1)
  .max(2)
  .required()
  .error(
    generateError('El numero de escalas debe tener entre 1 y 2 caracteres', 400)
  );

const precioFlightSchema = Joi.number()
  .precision(2)
  .required()
  .error(generateError('El precio del vuelo debe tener dos decimales', 400));

module.exports = {
  codigoFlightSchema,
  fecha_FlightSchema,
  hora_FlightSchema,
  escalasFlightSchema,
  precioFlightSchema
};
