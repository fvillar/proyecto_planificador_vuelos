const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

const {
  codigoAirlineSchema,
  nombreAirlineSchema,
  direccionAirlineSchema,
  codigo_postalAirlineSchema,
  ciudadAirlineSchema,
  provinciaAirlineSchema,
  paisAirlineSchema
} = require('./validationAirlineTemplates');

const airlineSchema = Joi.object().keys({
  codigo: codigoAirlineSchema,
  nombre: nombreAirlineSchema,
  direccion: direccionAirlineSchema,
  codigo_postal: codigo_postalAirlineSchema,
  ciudad: ciudadAirlineSchema,
  provincia: provinciaAirlineSchema,
  pais: paisAirlineSchema
});

const modifyAirlineSchema = Joi.object().keys({
  codigo: codigoAirlineSchema
});
module.exports = {
  airlineSchema,
  modifyAirlineSchema
};
