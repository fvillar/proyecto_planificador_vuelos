require('dotenv').config();

const { getConnection } = require('../db');
const {
  generateError,
  calculateFlightDuration,
  showFligthDataHumanFormat
} = require('../helpers');

const {
  newFlightSchema,
  getFlightsAirlineSchema,
  getFlightsAirportSchema,
  editFlightSchema
} = require('./validations/flightValidations');

const {
  searchFlights,
  searchFlightsIdaVuelta,
  fullSearchFlights
} = require('../searchHelpers/searchFlights');

async function newFlight(req, res, next) {
  let connection;
  try {
    //Comprobamos que quien va a introducir el vuelo es un proveedor o un administrador
    if (req.auth.rol !== 'proveedor' && req.auth.rol !== 'administrador') {
      throw generateError('No tienes permisos para añadir un nuevo vuelo', 401);
    }

    //Establecemos conexion con la base de datos
    connection = await getConnection();
    //Validamos los datos introducidos por el usuario
    await newFlightSchema.validateAsync(req.body);
    //Destructuramos los datos que nos vienen en el body
    const {
      codigo,
      codigo_aerolinea,
      codigo_aeropuerto_origen,
      codigo_aeropuerto_destino,
      fecha_salida,
      fecha_llegada,
      hora_salida,
      hora_llegada,
      escalas,
      precio
    } = req.body;

    //Calculamos la duracion del vuelo
    const duracion = calculateFlightDuration(
      fecha_llegada,
      hora_llegada,
      fecha_salida,
      hora_salida
    );

    //Obtenemos el id de la aerolinea y comprobamos que existe
    const [
      airlineData
    ] = await connection.query(`SELECT id FROM aerolineas WHERE codigo=?`, [
      codigo_aerolinea
    ]);
    if (!airlineData.length) {
      throw generateError('No existe ninguna aerolinea con este codigo', 400);
    }
    const id_aerolinea = airlineData[0].id;
    //Obtenemos el id del aeropuerto origen y comprobamos que existe
    const [
      airportOriginData
    ] = await connection.query(`SELECT id FROM aeropuertos WHERE codigo=?`, [
      codigo_aeropuerto_origen
    ]);
    if (!airportOriginData.length) {
      throw generateError(
        'No existe ningun aeropuerto de salida con este codigo',
        400
      );
    }
    const id_aeropuerto_origen = airportOriginData[0].id;
    //Obtenemos el id del aeropuerto destino y comprobamos que existe
    const [
      airportDestinationData
    ] = await connection.query(`SELECT id FROM aeropuertos WHERE codigo=?`, [
      codigo_aeropuerto_destino
    ]);
    if (!airportDestinationData.length) {
      throw generateError(
        'No existe ningun aeropuerto de llegada con este codigo',
        400
      );
    }
    const id_aeropuerto_destino = airportDestinationData[0].id;

    //Comprobamos que no exista el vuelo en la base de datos
    const [
      flightData
    ] = await connection.query(
      `SELECT id FROM vuelos WHERE codigo=? AND id_aerolinea=? AND id_aeropuerto_origen=? AND id_aeropuerto_destino=? AND fecha_salida=? AND fecha_llegada=? AND hora_salida=? AND hora_llegada=? AND numero_escalas=? AND precio=?`,
      [
        codigo,
        id_aerolinea,
        id_aeropuerto_origen,
        id_aeropuerto_destino,
        fecha_salida,
        fecha_llegada,
        hora_salida,
        hora_llegada,
        escalas,
        precio
      ]
    );

    if (flightData.length) {
      await connection.query(
        `UPDATE vuelos SET codigo=?, id_aerolinea=?, id_aeropuerto_origen=?, id_aeropuerto_destino=?, fecha_salida=?, fecha_llegada=?, hora_salida=?, hora_llegada=?, numero_escalas=?, precio=? WHERE id=?`,
        [
          codigo,
          id_aerolinea,
          id_aeropuerto_origen,
          id_aeropuerto_destino,
          fecha_salida,
          fecha_llegada,
          hora_salida,
          hora_llegada,
          escalas,
          precio,
          flightData[0].id
        ]
      );
    }

    if (!flightData.length) {
      await connection.query(
        `INSERT INTO vuelos ( codigo, id_aerolinea, id_aeropuerto_origen, id_aeropuerto_destino, fecha_salida, fecha_llegada, hora_salida, hora_llegada, duracion, numero_escalas, precio)
      VALUES (?,?,?,?,?,?,?,?,?,?,?) `,
        [
          codigo,
          id_aerolinea,
          id_aeropuerto_origen,
          id_aeropuerto_destino,
          fecha_salida,
          fecha_llegada,
          hora_salida,
          hora_llegada,
          duracion,
          escalas,
          precio
        ]
      );
    }
    res.send({
      staus: 'ok',
      message: 'Vuelo introducido correctamente en la base de datos.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function getAllFlights(req, res, next) {
  let connection;
  let showFlightData;

  try {
    connection = await getConnection();
    if (!!req.query.fecha_vuelta && req.query.fecha_vuelta !== '') {
      const {
        query,
        params,
        queryVuelta,
        paramsVuelta
      } = searchFlightsIdaVuelta(req.query);

      const resultIda = await connection.query(query, params);
      const [flightDataIda] = resultIda;
      if (!flightDataIda.length) {
        throw generateError(
          'No hay ningun vuelo de ida que cumpla los criterios de busqueda',
          404
        );
      }

      const resultVuelta = await connection.query(queryVuelta, paramsVuelta);
      const [flightDataVuelta] = resultVuelta;
      if (!flightDataVuelta.length) {
        throw generateError(
          'No hay ningun vuelo de vuelta que cumpla los criterios de busqueda',
          404
        );
      }

      showFlightData = {
        ida: showFligthDataHumanFormat(flightDataIda),
        vuelta: showFligthDataHumanFormat(flightDataVuelta)
      };
    } else {
      const { query, params } = searchFlights(req.query);
      const result = await connection.query(query, params);

      const [flightData] = result;

      if (!flightData.length) {
        throw generateError(
          'No hay ningun vuelo que cumpla los criterios de busqueda',
          404
        );
      }

      showFlightData = { ida: showFligthDataHumanFormat(flightData) };
    }
    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getFlightsByAllParams(req, res, next) {
  let connection;

  try {
    connection = await getConnection();
    const { query, params } = fullSearchFlights(req.query);
    const result = await connection.query(query, params);

    const [flightData] = result;

    if (!flightData.length) {
      throw generateError(
        'No hay ningun vuelo que cumpla los criterios de busqueda',
        401
      );
    }
    const showFlightData = showFligthDataHumanFormat(flightData);

    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getFlightsAirline(req, res, next) {
  let connection;

  try {
    await getFlightsAirlineSchema.validateAsync(req.params);
    const { codigo } = req.params;

    connection = await getConnection();

    //Comprobamos que existe la aerolinea
    const [
      existsAirline
    ] = await connection.query(`SELECT id from aerolineas WHERE codigo=?`, [
      codigo
    ]);
    if (!existsAirline.length) {
      throw generateError(
        'No exite en la base de datos ninguna aerolinea con el codigo indicado',
        400
      );
    }
    //Obtenemos los datos de los vuelos de la aerolinea
    const [flightData] = await connection.query(
      `SELECT v.id, v.codigo, al.codigo as aerolinea, apo.codigo as origen, apo.ciudad as ciudad_origen, apd.codigo as destino, apd.ciudad as ciudad_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada,v.duracion, v.numero_escalas, v.precio
      FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
      JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
      JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
      WHERE al.codigo=?
      ORDER BY fecha_salida`,
      [codigo]
    );
    //Comprobamos que la lista de vuelos no esté vacia
    if (!flightData.length) {
      throw generateError('La lista de vuelos de la aerolinea está vacia', 401);
    }
    //Convertimos la duracion del vuelo a un formato mas legible.
    const showFlightData = showFligthDataHumanFormat(flightData);

    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getFlightsAirport(req, res, next) {
  let connection;

  try {
    const { codigo } = req.params;
    await getFlightsAirportSchema.validateAsync(req.params);

    connection = await getConnection();

    //Comprobamos que existe el aeropuerto
    const [
      existsAirport
    ] = await connection.query(`SELECT id from aeropuertos WHERE codigo=?`, [
      codigo
    ]);
    if (!existsAirport.length) {
      throw generateError(
        'No exite en la base de datos ningun aeropuerto con el codigo indicado',
        400
      );
    }
    //Obtenemos los datos de los vuelos con origen en el aeropuerto
    const [flightData] = await connection.query(
      `SELECT v.id, v.codigo, al.codigo as aerolinea, apo.codigo as origen, apo.ciudad as ciudad_origen, apd.codigo as destino, apd.ciudad as ciudad_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada,v.duracion, v.numero_escalas, v.precio
      FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
      JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
      JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
      WHERE apo.codigo=?
      ORDER BY fecha_salida DESC `,
      [codigo]
    );
    //Comprobamos que la lista de vuelos no esté vacia
    if (!flightData.length) {
      throw generateError(
        `La lista de vuelos con origen en el aeropuerto con codigo ${codigo} está vacia`,
        401
      );
    }
    //Convertimos la duracion del vuelo a un formato mas legible.
    const showFlightData = showFligthDataHumanFormat(flightData);

    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getFlightsUser(req, res, next) {
  let connection;

  try {
    const { id } = req.params;
    //Comprobamos si es el usuario logueado o un administrador
    if (Number(id) !== req.auth.id && req.auth.rol !== 'administrador') {
      throw generateError('No tienes permisos para ver esta información', 401);
    }
    //Estabelecemos la conexion con la base de datos
    connection = await getConnection();

    //Obtenemos los datos de los vuelos del usuario
    const [flightData] = await connection.query(
      `SELECT u.id AS id_usuario, v.id AS id_uelo, v.codigo as codigo_vuelo, al.nombre AS aerolinea, apo.codigo AS origen,apo.ciudad AS ciudad_origen, apd.codigo AS destino, apd.ciudad AS ciudad_destino, v.fecha_salida as fecha_salida, v.fecha_llegada as fecha_llegada, v.hora_salida as hora_salida, v.hora_llegada as hora_llegada,v.duracion as duracion, v.numero_escalas as numero_escalas, v.precio as precio
      FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
      JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
      JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
      JOIN billetes b ON v.id=b.id_vuelo
      JOIN usuarios u ON b.id_usuario=u.id
      WHERE u.id=?
      ORDER BY fecha_salida DESC `,
      [id]
    );
    //Comprobamos que la lista de vuelos no esté vacia
    if (!flightData.length) {
      throw generateError(`Todavia no has volado con nosotros`, 401);
    }
    //Convertimos la duracion del vuelo a un formato mas legible.
    const showFlightData = showFligthDataHumanFormat(flightData);

    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getFlight(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    const result = await connection.query(
      `SELECT v.id, v.codigo, al.codigo as aerolinea, apo.codigo as origen, apo.ciudad as ciudad_origen, apd.codigo as destino, apd.ciudad as ciudad_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada,v.duracion, v.numero_escalas, v.precio
      FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
      JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
      JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
      WHERE v.id=?
      ORDER BY fecha_salida DESC `,
      [id]
    );

    const [flightData] = result;

    if (!flightData.length) {
      throw generateError(
        `El vuelo con id ${id} no existe en la base de datos`,
        401
      );
    }

    const showFlightData = showFligthDataHumanFormat(flightData);

    res.send({
      staus: 'ok',
      data: showFlightData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function editFlight(req, res, next) {
  let connection;
  try {
    //Comprobamos que el rol sea proveedor o administrador
    if (req.auth.rol !== 'proveedor' && req.auth.rol !== 'administrador') {
      throw generateError('No tienes permisos para editar el vuelo');
    }
    //Recogemos el id del vuelo a modificar
    const { id } = req.params;
    //Validamos los datos introducidos por el usuario
    await editFlightSchema.validateAsync(req.body);
    //Establecemos conexion con la base de datos
    connection = await getConnection();
    //Destructuramos los datos que nos vienen en el body
    const {
      codigo,
      codigo_aerolinea,
      codigo_aeropuerto_origen,
      codigo_aeropuerto_destino,
      fecha_salida,
      fecha_llegada,
      hora_salida,
      hora_llegada,
      escalas,
      precio
    } = req.body;

    //Calculamos la duracion del vuelo
    const duracion = calculateFlightDuration(
      fecha_llegada,
      hora_llegada,
      fecha_salida,
      hora_salida
    );
    //Comprobamos que exite el vuelo a modificar
    const [
      editFlight
    ] = await connection.query(`SELECT id FROM vuelos WHERE id=?`, [id]);
    if (!editFlight.length) {
      throw generateError(
        `El vuelo con id ${id} no existe en la base de datos.`,
        400
      );
    }
    //Obtenemos el id de la aerolinea y comprobamos que existe
    const [
      airlineData
    ] = await connection.query(`SELECT id FROM aerolineas WHERE codigo=?`, [
      codigo_aerolinea
    ]);
    if (!airlineData.length) {
      throw generateError('No existe ninguna aerolinea con este codigo', 400);
    }
    const id_aerolinea = airlineData[0].id;
    //Obtenemos el id del aeropuerto origen y comprobamos que existe
    const [
      airportOriginData
    ] = await connection.query(`SELECT id FROM aeropuertos WHERE codigo=?`, [
      codigo_aeropuerto_origen
    ]);
    if (!airportOriginData.length) {
      throw generateError(
        'No existe ningun aeropuerto de salida con este codigo',
        400
      );
    }
    const id_aeropuerto_origen = airportOriginData[0].id;
    //Obtenemos el id del aeropuerto destino y comprobamos que existe
    const [
      airportDestinationData
    ] = await connection.query(`SELECT id FROM aeropuertos WHERE codigo=?`, [
      codigo_aeropuerto_destino
    ]);
    if (!airportDestinationData.length) {
      throw generateError(
        'No existe ningun aeropuerto de llegada con este codigo',
        400
      );
    }
    const id_aeropuerto_destino = airportDestinationData[0].id;

    await connection.query(
      `UPDATE vuelos SET codigo=?, id_aerolinea=?, id_aeropuerto_origen=?, id_aeropuerto_destino=?, fecha_salida=?, fecha_llegada=?, hora_salida=?, hora_llegada=?, duracion=?, numero_escalas=?, precio=? WHERE id=?`,
      [
        codigo,
        id_aerolinea,
        id_aeropuerto_origen,
        id_aeropuerto_destino,
        fecha_salida,
        fecha_llegada,
        hora_salida,
        hora_llegada,
        duracion,
        escalas,
        precio,
        id
      ]
    );

    res.send({
      staus: 'ok',
      message: 'Vuelo modificado correctamente en la base de datos.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function deleteFlight(req, res, next) {
  let connection;
  try {
    //Recogemos el id del vuelo a eliminar
    const { id } = req.params;
    //Establecemos conexion con la base de datos
    connection = await getConnection();

    //Comprobamos que exite el vuelo a eliminar
    const [
      deleteFlight
    ] = await connection.query(`SELECT id FROM vuelos WHERE id=?`, [id]);
    if (!deleteFlight.length) {
      throw generateError(
        `El vuelo con id ${id} no existe en la base de datos.`,
        400
      );
    }

    //Comprobamos si ese vuelo tiene billetes asignados
    const [
      inUseFlight
    ] = await connection.query(`SELECT id FROM billetes WHERE id_vuelo=?`, [
      id
    ]);
    if (inUseFlight.length) {
      throw generateError(
        'Este vuelo tiene billetes asignados, por lo que no puede eliminarse',
        401
      );
    }

    await connection.query(`DELETE FROM vuelos WHERE id=?`, [id]);

    res.send({
      staus: 'ok',
      message: 'Vuelo eliminado correctamente en la base de datos.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  newFlight,
  getAllFlights,
  getFlightsByAllParams,
  getFlightsAirline,
  getFlightsAirport,
  getFlightsUser,
  getFlight,
  editFlight,
  deleteFlight
};
