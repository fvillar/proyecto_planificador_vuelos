require('dotenv').config();

const { getConnection } = require('../db');
const {
  newTicketSchema,
  editTicketSchema,
  deleteTicketSchema
} = require('./validations/ticketValidations');

const {
  generateError,
  sendTicketEmail,
  showFligthDataHumanFormat,
  randomString
} = require('../helpers');

const { searchTickets } = require('../searchHelpers/searchTickets');

async function newTicket(req, res, next) {
  let connection;
  try {
    //Establecemos conexion con la base de datos
    connection = await getConnection();
    //Obtenemos id del usuario a través del token
    const id_usuario = req.auth.id;
    //Validamos toda la informacion que nos llega en el body
    await newTicketSchema.validateAsync(req.body);
    //Recogemos todos los datos que nos llegan por el body
    const { id_vuelo, asiento } = req.body;

    //Comprobamos que existe el vuelo
    const [
      existsFlight
    ] = await connection.query('SELECT id FROM vuelos WHERE id=?', [id_vuelo]);
    if (!existsFlight.length) {
      throw generateError('El vuelo solicitado no existe');
    }

    //Comprobamos si el usuario indicado ya tiene un billete en ese vuelo
    const [
      existsTicket
    ] = await connection.query(
      'SELECT id FROM billetes WHERE id_usuario=? AND id_vuelo=?',
      [id_usuario, id_vuelo]
    );
    if (existsTicket.length) {
      throw generateError(
        'Este pasajero ya tiene un billete comprado para este vuelo'
      );
    }

    //Comprobamos que no haya un billete en ese vuelo con el mismo asiento ya asignado
    const [
      occupiedSeat
    ] = await connection.query(
      'SELECT id FROM billetes WHERE id_vuelo=? AND asiento=?',
      [id_vuelo, asiento]
    );
    if (occupiedSeat.length) {
      throw generateError(
        'El asiento solicitado ya está reservado en este vuelo.',
        400
      );
    }

    const codigo_reserva = randomString(25);

    //Insertamos los datos del billete en la tabla billetes de la base de datos
    await connection.query(
      'INSERT INTO billetes(id_usuario, id_vuelo, asiento, codigo_reserva) VALUES(?,?,?,?)',
      [id_usuario, id_vuelo, asiento, codigo_reserva]
    );

    //Enviamos mail de confirmacion de compra
    const [flightData] = await connection.query(
      `SELECT b.codigo_reserva, u.email, u.nombre, u.apellido_1, u.apellido_2, u.tipo_identificacion AS documento, u.numero_identificacion AS numero, apo.ciudad AS origen, apd.ciudad AS destino, al.nombre AS aerolinea, apo.codigo AS aeropuerto_origen, apd.codigo AS aeropuerto_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada, v.duracion, v.numero_escalas AS escalas, b.asiento FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
    JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
    JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
    JOIN billetes b ON v.id=b.id_vuelo
    JOIN usuarios u ON b.id_usuario=u.id WHERE b.id_usuario=? AND id_vuelo=?`,
      [id_usuario, id_vuelo]
    );

    const [ticketData] = showFligthDataHumanFormat(flightData);

    const ticketEmailData = `
    
      <h2 style="color:#163d70;text-align:center">Los datos de su vuelo son:</h2>
      <ul style="list-style:none; color:#163d70; margin-left:120px; padding-bottom:.5rem">
          <li style=""><h3>De ${ticketData.origen} (${ticketData.aeropuerto_origen}) a (${ticketData.destino})</h3></li>
          <li style=""><h3>Pasajero: ${ticketData.nombre} ${ticketData.apellido_1} ${ticketData.apellido_2}</h3></li>
          <li style="">codigo_reserva: ${ticketData.codigo_reserva}</li>
          <li style="">documento: ${ticketData.documento} ${ticketData.numero}</>
          <li style="">fecha_salida: ${ticketData.fecha_salida},</li>
          <li style="">hora_salida: ${ticketData.hora_salida},</li>
          <li style="">fecha_llegada: ${ticketData.fecha_llegada},</li>
          <li style="">hora_llegada: ${ticketData.hora_llegada},</li>
          <li style="">aerolinea: ${ticketData.aerolinea},</>
          <li style="">duracion:${ticketData.duracion},</li>
          <li style="">escalas: ${ticketData.escalas},</li>
          <li style="">asiento:${ticketData.asiento}</li>
      </ul>
    
    `;
    try {
      await sendTicketEmail({
        email: ticketData.email,
        title: 'Aquí tiene su billete de meuvoo',
        content: ticketEmailData
      });
    } catch (error) {
      console.error(error);
      throw generateError(
        'Se ha producido un error en el envío del mail. Prueba mas tarde',
        401
      );
    }

    const payload = {
      codigo_reserva: ticketData.codigo_reserva
    };

    //Damos respuesta si todo fue bien
    res.send({
      status: 'ok',
      message:
        'El billete ha sido creado con exito en la base de datos. Le hemos enviado un correo electronico con los datos del vuelo que ha comprado. No olvide revisar su carpeta de spam.',
      data: payload
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function getAllTickets(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { query, params } = searchTickets(req.query);
    const [result] = await connection.query(query, params);

    if (!result.length) {
      throw generateError('La lista de vuelos está vacia', 401);
    }

    const ticketData = showFligthDataHumanFormat(result);

    res.send({
      staus: 'ok',
      data: ticketData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getTicketsUser(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    if (Number(id) !== req.auth.id && req.auth.rol !== 'administrador') {
      throw generateError(
        'No tienes permisos para ver esta informacion. Contacta con un administrador'
      );
    }
    connection = await getConnection();

    const [result] = await connection.query(
      `SELECT u.nombre, u.apellido_1, u.apellido_2, u.tipo_identificacion AS documento, u.numero_identificacion AS numero, apo.ciudad AS origen, apd.ciudad AS destino, al.nombre AS aerolinea, apo.codigo AS aeropuerto_origen, apd.codigo AS aeropuerto_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada,v.duracion, v.numero_escalas AS escalas FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
    JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
    JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
    JOIN billetes b ON v.id=b.id_vuelo
    JOIN usuarios u ON b.id_usuario=u.id 
    WHERE b.id_usuario=?
    ORDER BY v.fecha_salida DESC`,
      [id]
    );

    if (!result.length) {
      throw generateError('La lista de vuelos está vacia', 401);
    }

    const ticketData = showFligthDataHumanFormat(result);

    res.send({
      staus: 'ok',
      data: ticketData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getTicket(req, res, next) {
  let connection;

  try {
    const { id_vuelo } = req.params;
    const id_usuario = req.auth.id;

    connection = await getConnection();

    const [
      existsTicket
    ] = await connection.query(
      'SELECT id FROM billetes WHERE id_usuario=? AND id_vuelo=?',
      [id_usuario, id_vuelo]
    );
    if (!existsTicket.length) {
      throw generateError('El vuelo solicitado no existe');
    }

    const [result] = await connection.query(
      `SELECT u.nombre, u.apellido_1, u.apellido_2, u.tipo_identificacion AS documento, u.numero_identificacion AS numero, apo.ciudad AS origen, apd.ciudad AS destino, al.nombre AS aerolinea, apo.codigo AS aeropuerto_origen, apd.codigo AS aeropuerto_destino, v.fecha_salida, v.fecha_llegada, v.hora_salida, v.hora_llegada,v.duracion, v.numero_escalas AS escalas FROM vuelos v JOIN aerolineas al ON v.id_aerolinea=al.id
    JOIN aeropuertos apo ON v.id_aeropuerto_origen=apo.id
    JOIN aeropuertos apd ON v.id_aeropuerto_destino=apd.id
    JOIN billetes b ON v.id=b.id_vuelo
    JOIN usuarios u ON b.id_usuario=u.id 
    WHERE b.id_usuario=? AND b.id_vuelo=?`,
      [id_usuario, id_vuelo]
    );

    if (!result.length) {
      throw generateError('La lista de vuelos está vacia', 401);
    }

    const [ticketData] = showFligthDataHumanFormat(result);

    res.send({
      staus: 'ok',
      data: ticketData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function editTicket(req, res, next) {
  let connection;
  try {
    //Validamos los datos introducidos por el usuario
    await editTicketSchema.validateAsync(req.body);

    //Establecemos conexion con la base de datos
    connection = await getConnection();

    //Destructuramos los datos que nos vienen en el body
    const { id_usuario, id_usuario_nuevo, id_vuelo } = req.body;

    //Comprobamos que exite el billete a modificar
    const [
      editTicket
    ] = await connection.query(
      `SELECT id FROM billetes WHERE id_usuario=? AND id_vuelo=?`,
      [id_usuario, id_vuelo]
    );

    if (!editTicket.length) {
      throw generateError(
        `El billete que quieres modificar no existe en la base de datos.`,
        400
      );
    }

    //Comprobamos que exite el nuevo usuario
    const [
      existsNewUser
    ] = await connection.query('SELECT id FROM usuarios WHERE id=?', [
      id_usuario_nuevo
    ]);
    if (!existsNewUser.length) {
      throw generateError(
        'No existe el usuario al que se le quiere asignar el billete.',
        401
      );
    }

    //Comprobamos que el nuevo usuario no tenga ya un billete en el vuelo
    const [
      existsTicket
    ] = await connection.query(
      'SELECT id_usuario, id_vuelo FROM billetes WHERE id_usuario=? AND id_vuelo=?',
      [id_usuario_nuevo, id_vuelo]
    );

    if (existsTicket.length) {
      throw generateError(
        'El nuevo usuario ya tiene un billete en este vuelo',
        400
      );
    }

    //Obtenemos el id del billete a modificar y actualizamos la base de datos con el nuevo usuario
    const idCurrentTicket = editTicket[0].id;
    await connection.query(
      `UPDATE billetes SET id_usuario=?, id_vuelo=? WHERE id=?`,
      [id_usuario_nuevo, id_vuelo, idCurrentTicket]
    );

    res.send({
      staus: 'ok',
      message: 'Billete modificado correctamente en la base de datos.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function deleteTicket(req, res, next) {
  let connection;
  try {
    //Validamos los datos introducidos por el usuario
    await deleteTicketSchema.validateAsync(req.body);
    //Recogemos del body los datos del billete a eliminar
    const { id_usuario, id_vuelo } = req.body;
    //Establecemos conexion con la base de datos
    connection = await getConnection();

    //Comprobamos que exite el billete a eliminar
    const [
      deleteTicket
    ] = await connection.query(
      `SELECT id FROM billetes WHERE id_usuario=? AND id_vuelo=?`,
      [id_usuario, id_vuelo]
    );
    if (!deleteTicket.length) {
      throw generateError(
        `El billete a eliminar no existe en la base de datos.`,
        400
      );
    }
    const idCurrentTicket = deleteTicket[0].id;
    //Eliminamos billete de la base de datos
    await connection.query(`DELETE FROM billetes WHERE id=?`, [
      idCurrentTicket
    ]);

    res.send({
      staus: 'ok',
      message: 'Billete eliminado correctamente en la base de datos.'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  newTicket,
  getAllTickets,
  getTicketsUser,
  getTicket,
  editTicket,
  deleteTicket
};
