require('dotenv').config();

const { generateError } = require('../helpers');
const { getConnection } = require('../db');

const {
  airlineSchema,
  modifyAirlineSchema
} = require('./validations/airlineValidations');

const { searchAirlines } = require('../searchHelpers/searchAirlines');

async function newAirline(req, res, next) {
  let connection;

  try {
    //Establecemos conecxion con la base de datos
    connection = await getConnection();

    //Validamos los datos introducidos por el usuario
    await airlineSchema.validateAsync(req.body);

    //Destructuramos los datos que nos vienen en el body
    const {
      codigo,
      nombre,
      direccion,
      codigo_postal,
      ciudad,
      provincia,
      pais
    } = req.body;

    //Comprobamos si ya existe la aerolinea en nuestra base de datos
    const [
      existingAirline
    ] = await connection.query('SELECT id from aerolineas where codigo=?', [
      codigo
    ]);

    if (existingAirline.length) {
      throw generateError('Esta aerolinea ya existe en la base de datos', 409);
    }

    await connection.query(
      `INSERT INTO aerolineas ( codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais )
      VALUES (?,?,?,?,?,?,?)`,
      [codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais]
    );

    res.send({
      staus: 'ok',
      message: 'Aerolinea incorporada a la base de datos'
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function getAllAirlines(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { query, params } = searchAirlines(req.query);
    const result = await connection.query(query, params);

    const [airlineData] = result;

    if (!airlineData.length) {
      throw generateError('La lista de aerolineas está vacia', 401);
    }

    res.send({
      staus: 'ok',
      data: airlineData
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function getAirline(req, res, next) {
  let connection;

  try {
    const { id } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
      SELECT id, codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais, activo, fecha_creacion, fecha_modificacion
      FROM aerolineas WHERE id=?  `,
      [id]
    );

    // Configuramos error por si no hay resultados a la consulta
    if (!result.length) {
      throw generateError(`No existe la aerolinea con id ${id}`, 404);
    }

    const [airlineData] = result;

    const payload = {
      id: airlineData.id,
      codigo: airlineData.codigo,
      nombre: airlineData.nombre,
      direccion: airlineData.direccion,
      codigo_postal: airlineData.codigo_postal,
      ciudad: airlineData.ciudad,
      provincia: airlineData.provincia,
      pais: airlineData.pais,
      activo: airlineData.activo,
      creacion: airlineData.fecha_creacion,
      modificacion: airlineData.fecha_modificacion
    };

    res.send({
      status: 'ok',
      data: payload
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function editAirline(req, res, next) {
  let connection;

  try {
    await airlineSchema.validateAsync(req.body);

    const { id } = req.params;

    const {
      codigo,
      nombre,
      direccion,
      codigo_postal,
      ciudad,
      provincia,
      pais
    } = req.body;

    connection = await getConnection();

    // Comprobamos si existe el id y la aerolinea
    const [current] = await connection.query(
      `
      SELECT 
      id, codigo
      FROM aerolineas 
      WHERE id=?`,
      [id]
    );

    if (!current.length) {
      throw generateError(
        `No existe la aerolinea con el codigo ${codigo}`,
        404
      );
    }

    // Actualizamos la aerolinea
    await connection.query(
      `UPDATE aerolineas 
        SET 
        codigo=?,
        nombre=?,
        direccion=?,
        codigo_postal=?,
        ciudad=?,
        provincia=?,
        pais=?
        WHERE id=?`,
      [codigo, nombre, direccion, codigo_postal, ciudad, provincia, pais, id]
    );

    res.send({ status: 'ok', message: 'Aerolinea actualizada correctamente' });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function deleteAirline(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    // Comprobamos si existe la aerolinea
    const [current] = await connection.query(
      `
      SELECT 
      id, codigo
      FROM aerolineas 
      WHERE id=?`,
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe esta aerolinea`, 404);
    }

    await connection.query('DELETE FROM aerolineas WHERE id=?', [id]);

    res.send({
      status: 'ok',
      message: `La aerolinea ha sido eliminada de la base de datos`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function disableAirline(req, res, next) {
  let connection;
  try {
    const { id } = req.params;

    connection = await getConnection();

    const [
      current
    ] = await connection.query(
      'SELECT id, codigo, activo from aerolineas where id=? AND activo=1',
      [id]
    );

    if (!current.length) {
      throw generateError(`No existe esta aerolinea o no está activa`, 400);
    }

    await connection.query('UPDATE aerolineas SET activo=0 WHERE id=?', [id]);

    res.send({
      status: 'ok',
      message: `La aerolinea ha sido deshabilitada correctamente`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

async function enableAirline(req, res, next) {
  let connection;

  try {
    const { id } = req.params;

    connection = await getConnection();

    // Buscamos la aerolinea en la base de datos
    const [
      current
    ] = await connection.query(
      'SELECT id, codigo, activo from aerolineas where id=? AND activo=0',
      [id]
    );

    if (!current.length) {
      throw generateError(
        `No existe ninguna aerolinea deshabilitada con esos datos`,
        404
      );
    }

    await connection.query(
      `UPDATE aerolineas SET activo=1 WHERE id=?
    `,
      [id]
    );

    // Creamos respuesta
    res.send({
      status: 'ok',
      message: `La aerolinea con codigo ha sido habilitada correctamente`
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = {
  newAirline,
  getAllAirlines,
  getAirline,
  editAirline,
  deleteAirline,
  disableAirline,
  enableAirline
};
