require('dotenv').config();
const bcrypt = require('bcrypt');

const { getConnection } = require('./db');
const {
  initialAirlinesQuery,
  initialAirportsQuery
} = require('./initDBQuerys');

async function initialDB() {
  const connection = await getConnection();

  console.log('Borrando tablas si existen');
  await connection.query('DROP TABLE IF EXISTS billetes');
  await connection.query('DROP TABLE IF EXISTS vuelos');
  await connection.query('DROP TABLE IF EXISTS aerolineas');
  await connection.query('DROP TABLE IF EXISTS aeropuertos');
  await connection.query('DROP TABLE IF EXISTS usuarios');

  console.log('Creando tablas de BB.DD.');
  await connection.query(`
    create table usuarios ( 
      id int unsigned auto_increment,
      email varchar(255) not null,
      password varchar(255) not null,
      rol enum('administrador','usuario','proveedor'),
      activo boolean default false,
      codigo_registro varchar(255),
      nombre varchar(255) default 'Nombre',
      apellido_1 varchar(255) default 'Apellido',
      apellido_2 varchar(255) default 'Apellido',
      tipo_identificacion varchar(20) default 'Tipo_identificacion',
      numero_identificacion varchar(30) default 'Numero_identificacion',
      direccion varchar(255) default 'Direccion usuario',
      codigo_postal varchar(20) default 'Codigo postal',    
      ciudad varchar(100) default 'Ciudad',
      provincia varchar(100) default 'Provincia',
      pais varchar(100) default 'Pais',
      fecha_nacimiento date default "1900-01-01",
      telefono varchar(30) default 'Telefono',
      foto varchar(255),
      numero_vuelos int default 0,
      fecha_cambio_password datetime default now(),
      fecha_creacion timestamp not null default current_timestamp,
      fecha_modificacion timestamp not null default current_timestamp on update current_timestamp,
      constraint PK_usuarios primary key (id)
    )
    `);

  await connection.query(`
    create table aeropuertos( 
    id int unsigned auto_increment,
    codigo varchar(20) not null,
    nombre varchar(255),
    direccion varchar(255) default 'Direccion aeropuerto',
    codigo_postal varchar(20) default 'Codigo postal',
    ciudad varchar(100),
    provincia varchar(100) default 'Provincia',
    pais varchar(100),
    activo boolean default true,
    fecha_creacion timestamp not null default current_timestamp,
    fecha_modificacion timestamp not null default current_timestamp on update current_timestamp,
    constraint PK_aeropuertos primary key (id)
)
  `);

  await connection.query(`
  create table aerolineas(
    id int unsigned auto_increment,
    codigo varchar(20) not null,
    nombre varchar(255),
    direccion varchar(255) default 'Direccion aerolinea',
    codigo_postal varchar(20) default 'Codigo postal',
    ciudad varchar(100) default 'Ciudad',
    provincia varchar(100) default 'Provincia',    
    pais varchar(100),
    activo boolean default true,
    fecha_creacion timestamp not null default current_timestamp,
    fecha_modificacion timestamp not null default current_timestamp on update current_timestamp,
    constraint PK_aerolineas primary key (id)
)
  `);

  await connection.query(`
  create table vuelos(
    id int unsigned auto_increment,
    codigo varchar(20) not null,
    id_aerolinea int unsigned,
    id_aeropuerto_origen int unsigned ,
    id_aeropuerto_destino int unsigned,
    fecha_salida date not null,
    fecha_llegada date not null,
    hora_salida time,
    hora_llegada time,
    duracion int unsigned,
    numero_escalas int not null default 0,
    precio decimal(10,2),
    fecha_creacion timestamp not null default current_timestamp,
    fecha_modificacion timestamp not null default current_timestamp on update current_timestamp,
    constraint PK_vuelos primary key (id)
    )
  `);

  await connection.query(`
    alter table vuelos
      add constraint FK_vuelos_aerolineas
      foreign key (id_aerolinea)
      references aerolineas(id)
      on update cascade
      on delete set null;
  `);

  await connection.query(`
    alter table vuelos
      add constraint FK_vuelos_aeropuerto_origen
      foreign key (id_aeropuerto_origen)
      references aeropuertos(id)
      on update cascade
      on delete set null;
  `);

  await connection.query(`
    alter table vuelos
      add constraint FK_vuelos_aeropuerto_destino
      foreign key (id_aeropuerto_destino)
      references aeropuertos(id)
      on update cascade
      on delete set null;
  `);

  await connection.query(`
    create table billetes(
      id int unsigned auto_increment,
      id_usuario int unsigned,
      id_vuelo int unsigned,
      asiento varchar(10),
      codigo_reserva varchar(255),
      fecha_creacion timestamp not null default current_timestamp,
      fecha_modificacion timestamp not null default current_timestamp on update current_timestamp,
      constraint PK_billetes primary key (id)
)
  `);

  await connection.query(`
    alter table billetes
      add constraint FK_billetes_usuario
      foreign key (id_usuario)
      references usuarios(id)
      on update cascade
      on delete set null;
  `);

  await connection.query(`
    alter table billetes
      add constraint FK_billetes_vuelos
      foreign key (id_vuelo)
      references vuelos(id)
      on update cascade
      on delete set null;
  `);

  // Create initial user
  const adminPassword = await bcrypt.hash(
    process.env.DEFAULT_ADMIN_PASSWORD,
    10
  );
  const userPassword = await bcrypt.hash(process.env.DEFAULT_USER_PASSWORD, 10);

  await connection.query(`
        INSERT INTO usuarios(email, rol, password, activo, nombre, apellido_1, ciudad, fecha_nacimiento) VALUES
        ("pmeuvoo@gmail.com", "administrador",  "${adminPassword}", "1", "Felix", "Villar", "A Coruña", "1980-06-08"),
        ("pmeuvoo+us1@gmail.com", "usuario",  "${userPassword}", "1", "Gillermo", "Taboada", "A Coruña", "2000-04-01"),
        ("pmeuvoo+us2@gmail.com", "usuario",  "${userPassword}", "1", "Santiago", "Villaverde", "A Coruña", "2000-01-01"),
        ("pmeuvoo+us3@gmail.com", "usuario",  "${userPassword}", "1", "Sara", "Laso", "A Coruña", "2000-02-01")
      `);
  await connection.query(initialAirlinesQuery);
  await connection.query(initialAirportsQuery);

  connection.release();
  process.exit();
}

initialDB();
