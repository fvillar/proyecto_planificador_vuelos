# proyecto_planificador_vuelos
TÍTULO
PLANIFICADOR DE VUELOS
DESCRIPCIÓN
Plataforma para búsqueda de vuelos con filtros como duración, números de
paradas y precio (destino y fechas).
La plataforma permite:
USUARIOS ANÓNIMOS:
● Visualizar la landing
● Búsqueda vuelos por:
● Destino
● Fechas
● Duración del vuelo
● Número de paradas
● Rango de precios
■ Login
■ Registro (le llega email de registro)
USUARIOS REGISTRADOS:
● Gestión del perfil (cambios...)
■ Búsqueda vuelos por:
● Destino
● Fechas
● Duración del vuelo
● Número de paradas
● Rango de precios
● Reserva del vuelo (le llega email de confirmación)
INFORMACIÓN DEL PROVEEDOR:
● La plataforma permitirá introducir la información necesaria sobre los
vuelos. Preferiblemente utilizando APIs de terceros (AMADEUS,
KAYAK...). Si por dificultad se descarta utilizar APIs de terceros, se
recomienda preparar un único fichero JSON con los datos necesarios de
los vuelos.
