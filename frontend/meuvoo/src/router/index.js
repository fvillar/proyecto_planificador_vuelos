import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import { isLoggedIn } from "../helpers/helpers";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue"),
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../views/Profile.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: false,
    },
  },
  {
    path: "/*",
    name: "Error",
    component: () => import("../views/Error.vue"),
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
];

const router = new VueRouter({
  routes,
});

//COMPROBANDO CADA PÁGINA POR SI LA PERSONA ESTÁ LOGUEADA
router.beforeEach((to, from, next) => {
  //SI LA RUTA ES PRIVADA Y LA PERSONA NO TIENE TOKEN
  if (!to.meta.allowAnonymous && !isLoggedIn()) {
    next({
      path: "/",
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
