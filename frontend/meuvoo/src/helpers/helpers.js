import axios from "axios";
import jwtDecode from "jwt-decode";

export const ENDPOINT = "http://localhost:3001";
const AUTH_TOKEN_KEY = "authToken";

//FUNCION DE LOGIN
export function loginUser(email, password) {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await axios({
        url: `${ENDPOINT}/users/login`, //URL DE LA AUTENTICACION
        method: "POST", //METODO DE LA AUTENTICACION
        data: {
          email: email,
          password: password,
        }, //DATOS DE LA AUTENTICACION
      });
      setAuthToken(res.data.data.token);
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

//GUARDAR TOKEN EN LOCALSTORAGE
export function setAuthToken(token) {
  axios.defaults.headers.common["authorization"] = `Bearer ${token}`;
  localStorage.setItem(AUTH_TOKEN_KEY, token);
}

//LOGOUT
export function clearLogin() {
  axios.defaults.headers.common["authorization"] = "";
  localStorage.removeItem(AUTH_TOKEN_KEY);
}

//COGER EL TOKEN DEL LOCAL STORAGE
export function getAuthToken() {
  return localStorage.getItem(AUTH_TOKEN_KEY);
}

//COMPROBAR SI EL USER ESTÁ LOGUEADO O NO
export function isLoggedIn() {
  let authToken = getAuthToken();
  return !!authToken && !isTokenExpired(authToken);
}

//COMPROBAMOS SI ES VALIDA LA FECHA DEL TOKEN
export function isTokenExpired(token) {
  let expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

//COMPROBAR Y DEVOLVER FECHA EXPIRACION TOKEN
export function getTokenExpirationDate(encodedToken) {
  let token = jwtDecode(encodedToken);
  //SI NO HAY, NO SIGUE
  if (!token.exp) {
    return null;
  }
  let date = new Date(0);
  date.setUTCSeconds(token.exp);
  return date;
}

//EXTRAER INFORMACION DEL TOKEN
export function getFromToken(param) {
  const encodedToken = getAuthToken();
  let token;
  if (!!encodedToken && !isTokenExpired(encodedToken)) {
    token = jwtDecode(encodedToken);
    if (!token[param]) {
      return null;
    } else {
      return token[param];
    }
  }
  return null;
}

//FUNCION PARA OBTENER NUMERO ALEATORIO ENTRE UN MIN Y UN MAX
export function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
//FUNCION QUE ASIGNA ASIENTO ALEATORIO
export function asignRandomSeat() {
  const row = getRandomNumber(1, 36);
  const column = ["A", "B", "C", "D", "E", "F"];
  const letter = column[getRandomNumber(0, 5)];
  return `${row}${letter}`;
}
